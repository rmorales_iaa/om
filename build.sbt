//=============================================================================
//Start of file 'build.sbt'
//=============================================================================
//add settings to be added to BuildInfoKey (see below) to be available in scala source
lazy val programName    = "om"
lazy val programVersion = "0.0.1"
lazy val authorList     = "Rafael Morales Muñoz (rmorales@iaa.es)"
lazy val license        = "This project is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)"

//-----------------------------------------------------------------------------
//version will be calculated using git repository
val myScalaVersion     = "2.13.14"
lazy val commonSettings = Seq(
  name                 := programName
  , version            := programVersion
  , scalaVersion       := myScalaVersion
  , description        := s"$programName (observing manager). Manager of the FITS files obtained in telescope observings"
  , organization       := "IAA-CSIC"
  , homepage           := Some(url("http://www.iaa.csic.es"))
)
//=============================================================================
//Main class
lazy val mainClassName = s"com.$programName.Main"
//-----------------------------------------------------------------------------
//https://github.com/benblack86/finest
//To help improve code quality the following compiler flags will provide errors for deprecated and lint issues
scalacOptions ++= Seq(
  "-Xlint",
  "-deprecation",
  "-feature",
  "-unchecked"
)
//=============================================================================
//versions (get the lastest version from https://mvnrepository.com/)
val apacheCommonsFileUtils   = "2.16.1"
val apacheCommonsCsvVersion  = "1.11.0"
val apacheCommonsMath        = "3.6.1"
val apacheLogCoreVersion     = "2.19.0"
val apacheLogScalaVersion    = "13.1.0"
val commandLineParserVersion = "5.1.0"
val jamaVersion              = "1.0.3"
val javaFitsVersion          = "1.20.1"
val javaxMailVersion         = "1.6.2"
val jsonParsingUpickleVersion= "4.0.1"
val mongoScalaDriverVersion  = "5.2.0"
val postgreSQL_Version       = "42.7.4"
val qosLogbackVersion        = "1.5.8"
val scalaSqlVersion          = "0.1.9"
val scalajHttpVersion        = "2.4.2"
val sshVersion               = "0.2.20"
val typeSafeVersion          = "1.4.3"
//=============================================================================
//assembly
assembly / mainClass := Some(mainClassName)
assembly / logLevel  := Level.Warn

assembly / assemblyMergeStrategy := {
  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  case _ => MergeStrategy.first
}
//=============================================================================
//exclude directories form source path: github.com/sbt/sbt-jshint/issues/14
Compile / unmanagedSources / excludeFilter := {
  new SimpleFileFilter(f=> {
    val p = f.getCanonicalPath  

    p.contains("coordinate/spherical") ||
    p.contains("database/simbad") ||
    p.contains("database/vizier") ||
    p.contains("database/mongoDB") ||
    p.contains("fits/metaData") ||
    p.contains("fits/standard") ||
    p.contains("fits/wcs/fit") ||
    p.contains("image/astrometry") ||
    p.contains("image/catalog") ||  
    p.contains("image/estimator/flux/") ||
    p.contains("image/estimator/skyPosition/") ||
    p.contains("image/focusType") ||
    p.contains("image/fumo/") ||
    p.contains("image/mpo") ||
    p.contains("image/myImage/dir") ||
    p.contains("image/nsaImage") ||    
    p.contains("image/occultation") || 
    p.contains("image/photometry") ||   
    p.contains("image/registration") || 
    p.contains("image/script") ||    
    p.contains("image/web") ||    
    p.contains("myJava/nom/tam") ||
    p.contains("util/computerInfo") || 
    p.contains("util/parser")
  })}

//=============================================================================
//dependence list
lazy val dependenceList = Seq(

  //manage configuration files. https://github.com/typesafehub/config
  "com.typesafe" % "config" % typeSafeVersion

  //logging: https://logging.apache.org/log4j/2.x/manual/scala-api.html
  , "org.apache.logging.log4j" %% "log4j-api-scala" % apacheLogScalaVersion
  , "org.apache.logging.log4j" % "log4j-core" % apacheLogCoreVersion % Runtime
  , "ch.qos.logback" % "logback-classic" % qosLogbackVersion //required by mongoDB

  //command line argument parser: https://github.com/scallop/scallop
  , "org.rogach" %% "scallop" % commandLineParserVersion

  //apache commons file utils
  , "commons-io" % "commons-io" % apacheCommonsFileUtils

  //apache commons; CSV management
  , "org.apache.commons" % "commons-csv" % apacheCommonsCsvVersion

  //postgreSQL: http://www.lihaoyi.com/post/ScalaSqlaNewSQLDatabaseQueryLibraryforthecomlihaoyiScalaEcosystem.html#the-missing-database-library-in-the-com-lihaoyi-ecosystem
  , "com.lihaoyi" %% "scalasql" % scalaSqlVersion
  , "com.lihaoyi" %% "scalasql-core" % scalaSqlVersion
  , "com.lihaoyi" %% "scalasql-operations" % scalaSqlVersion
  , "com.lihaoyi" %% "scalasql-query"  % scalaSqlVersion
  , "org.postgresql" % "postgresql" % postgreSQL_Version

  //scala reflect
  , "org.scala-lang" % "scala-reflect" % myScalaVersion

  //scala wrapper for HttpURLConnection. OAuth included. https://github.com/scalaj/scalaj-http
  , "org.scalaj" %% "scalaj-http" % scalajHttpVersion

  //json parsing: // https://mvnrepository.com/artifact/com.lihaoyi/upickle
  , "com.lihaoyi" %% "upickle" % jsonParsingUpickleVersion

  // basic linear algebra package for Java.  https://mvnrepository.com/artifact/gov.nist.math/jama
  , "gov.nist.math" % "jama" % jamaVersion

  //https://mvnrepository.com/artifact/org.apache.commons/commons-math3 https://mvnrepository.com/artifact/org.apache.commons/commons-math3c
  , "org.apache.commons" % "commons-math3" % apacheCommonsMath

  //java FITS: https://mvnrepository.com/artifact/gov.nasa.gsfc.heasarc/nom-tam-fits
  , "gov.nasa.gsfc.heasarc" % "nom-tam-fits" % javaFitsVersion

  //ssh:http://www.jcraft.com/jsch/
  , "com.github.mwiede" % "jsch" % sshVersion

  //mail agent: https://javaee.github.io/javamail/
  , "com.sun.mail" % "javax.mail" % javaxMailVersion

  //mongoDB
  , "org.mongodb.scala" %% "mongo-scala-driver" % mongoScalaDriverVersion
)
//=============================================================================
//https://github.com/benblack86/finest
//To help improve code quality the following compiler flags will provide errors for deprecated and lint issues
scalacOptions ++= Seq (
  "-Xlint",
  "-deprecation",
  "-feature",
  "-unchecked"
  //  "-Xfatal-warnings"   //rotationalPeriod an error when warning raises. Enable when stable version was published
)
//=============================================================================
//git versioning: https://github.com/sbt/sbt-git

git.useGitDescribe := true
git.uncommittedSignifier := None         //do not append suffix is some changes are not pushed to git repo
git.gitDescribePatterns := Seq("v*.*")   //format of the tag version

//https://stackoverflow.com/questions/56588228/how-to-get-git-gittagtoversionnumber-value
git.gitTagToVersionNumber := { tag: String =>
  if (tag.isEmpty)  None
  else Some(tag.split("-").toList.head)
}

//See configuration options in: https://github.com/sbt/sbt-buildinfo

//package and name of the scala code auto-generated
buildInfoPackage := "BuildInfo"
buildInfoObject := "BuildInfo"

//append build time
buildInfoOptions += BuildInfoOption.BuildTime

//append extra options to be generated
buildInfoKeys ++= Seq[BuildInfoKey](
  buildInfoBuildNumber
  , scalaVersion
  , sbtVersion
  , name
  , version
  , "authorList" -> authorList
  , "license" -> license
  , description
  , organization
  , homepage
  , BuildInfoKey.action("myGitCurrentBranch") {git.gitCurrentBranch.value}
  , BuildInfoKey.action("myGitHeadCommit")    {git.gitHeadCommit.value.get}
)
//=============================================================================
//root project
lazy val root = (project in file("."))
  .settings(commonSettings: _*)
  .enablePlugins(AssemblyPlugin, BuildInfoPlugin, GitVersioning)
  .settings(libraryDependencies ++= dependenceList)
//-----------------------------------------------------------------------------
//End of file 'build.sbt'
//-----------------------------------------------------------------------------
