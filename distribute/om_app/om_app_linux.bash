#!/bin/bash
#------------------------------------------------------------------------------
#user variable section
APPLICATION_NAME=om
#------------------------------------------------------------------------------
#Variable section
UPDATER_JAR=updater.jar
APP_JAR=$APPLICATION_NAME.jar
#------------------------------------------------------------------------------
#call updater
cd app/updater
java -jar $UPDATER_JAR
cd ..
cd ..
#------------------------------------------------------------------------------
#call program
cd app/$APPLICATION_NAME
java -jar $APP_JAR "$@"
#------------------------------------------------------------------------------
#end of file
