
REM ------------------------------------------------------------------------------
REM user variable section
SET APPLICATION_NAME=om
REM ------------------------------------------------------------------------------
REM Variable section
SET UPDATER_JAR=updater.jar
SET APP_JAR=%APPLICATION_NAME%.jar
REM ------------------------------------------------------------------------------
REM call updater
cd app/updater
java -jar %UPDATER_JAR%
cd ..
cd ..
REM ------------------------------------------------------------------------------
REM call program
cd app/%APPLICATION_NAME%
java -jar %APP_JAR% %*
REM ------------------------------------------------------------------------------
REM end of file
