/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  03/dic/2018
  * Time:  01h:38m
  * Description: https://github.com/scallop/scallop
  */
//=============================================================================
package com.om.commandLine
//=============================================================================
import com.om.Version
import com.om.commandLine.CommandLineParser.COMMAND_ADD_CAL
import org.rogach.scallop._
//=============================================================================
import com.common.util.file.MyFile._
import com.common.util.path.Path._
//=============================================================================
//=============================================================================
object CommandLineParser {
  //---------------------------------------------------------------------------
  final val COMMAND_VERSION                         = "version"
  final val COMMAND_EXPLORE                         = "explore"
  final val COMMAND_ADD_CAL                         = "add-cal"
  final val COMMAND_ADD_CAL_LABEL                   = "add-cal-label"
  final val COMMAND_INFO                            = "info"
  final val COMMAND_INFO_SAVE                       = "info-save"
  final val COMMAND_REMOTE_SYNC_CAL                 = "remote-sync-cal"
  final val COMMAND_REMOTE_DIR_NOT_CAL              = "remote-dir-not-cal"
  final val COMMAND_REPORT_IMAGE_INFO               = "report-image-info"
  //---------------------------------------------------------------------------
  final val VALID_COMMAND_SEQ = Seq(
      COMMAND_VERSION
    , COMMAND_EXPLORE
    , COMMAND_ADD_CAL
    , COMMAND_ADD_CAL_LABEL
    , COMMAND_INFO
    , COMMAND_INFO_SAVE
    , COMMAND_REMOTE_SYNC_CAL
    , COMMAND_REMOTE_DIR_NOT_CAL
    , COMMAND_REPORT_IMAGE_INFO)
  //---------------------------------------------------------------------------
}
//=============================================================================
//-----------------------------------------------------------------------------
import CommandLineParser._
class CommandLineParser(args: Array[String]) extends ScallopConf(args) {
  version(Version.value + "\n")
  banner(s"""om syntax
           |om [--configuration-file configurationFile] COMMAND COMMAND_PARAMETERS
           |\t[$COMMAND_VERSION]
           |\t[$COMMAND_EXPLORE include-dir-seq exclude-dir-seq]
           |\t[$COMMAND_ADD_CAL include-dir-seq exclude-dir-seq --output-dir]
           |\t[$COMMAND_ADD_CAL_LABEL inDir outDir]
           |\t[$COMMAND_INFO fileName]
           |\t[$COMMAND_INFO_SAVE fileName]
           |\t[$COMMAND_REMOTE_SYNC_CAL dir]
           |\t[$COMMAND_REMOTE_DIR_NOT_CAL date/days file]
           | #---------------------------------------------------------------------------------------------------------
           | Example 0: Show version
           |  java -jar om.jar --$COMMAND_VERSION
           |
           | #---------------------------------------------------------------------------------------------------------
           | Example 1: Explore all records of the FITS files headers stored in the sequence directories (recursively) 'dir1' and 'dir2'
           |            excluding the directories 'dir0'. The result is a table definition for all records. Using the database 'db' and main table 'tn'
           |            If 'db' and 'tb' are not specified, the ones specified in the database will be used-,
           |  java -jar om.jar --$COMMAND_EXPLORE "dir1 dir2" --exclude-dir-seq "dir3" --database-table db:tn
           |
           | #---------------------------------------------------------------------------------------------------------
           | Example 2: Add (or create if its needed) a table in a database, both defined in the configuration file. This table contains
           |           all records of the FITS files headers stored in the sequence directories (recursively) 'dir1' and 'dir2'
           |           excluding the directories 'dir0'. For all images, using its header, are identified its type (bias,flat,science)
           |           Some images will not properly identified and will be ignored and reported in the log file.
           |
           |           All bias and flat images corresponding to the same observing night (calculated from its timestamp)
           |           are grouped (flat is grouped one time more according to its filter) creating the "master_bias" and the "master_flat".
           |           After adding to the database, the science image are calibrated subtracting the "master_bias"
           |           images and dividing by "master_flat". To find the proper "masters" that matches with the science
           |           image will be used the following parameters: xPixDimension, yPixDimension, telescope filter and
           |           the closest observing night. Using the timestamp of the science image, will be considered the masters
           |           with observing night around the timestamp plus the configuration file parameter "Calibration.maxDayDifferenceAllowedForMatching"
           |           and was selected the closest one. When no "master_flat" / "master_bias" is found, it is assumed an image
           |           with all pixels to zero / one.
           |           Using the database 'db' and main table 'tn'.
           |           If 'db' and 'tb' are not specified, the ones specified in the database will be used.
           |
           |  java -jar om.jar --$COMMAND_ADD_CAL "dir1 dir2" --exclude-dir-seq "dir3" --output-image-dir "output"  --database-table db:tn
           |
           | #---------------------------------------------------------------------------------------------------------
           | Example 2.1: Same as '$COMMAND_ADD_CAL' but performing a '$COMMAND_EXPLORE' and dropping the main and calibration table.
           |
           |  java -jar om.jar --$COMMAND_ADD_CAL "dir1 dir2" --output-image-dir "output"  --database-table db:tn --drop
           |
           | #---------------------------------------------------------------------------------------------------------
           | Example 3: Add calibration labels to all files of a directory 'in' and put the results on directory 'out'.
           | Assuming table name: 'om_test' and year: 'no_year'
           |  java -jar om.jar --$COMMAND_ADD_CAL_LABEL in --output-image-dir out
           |
           |#---------------------------------------------------------------------------------------------------------
           | Example 4: Ge the related info of image 'img' using the database 'om_cal' and table '2004.calibration'
           |            used to perform the calibration of the a science.
           |            If '2024' database and '2004.calibration' table are not specified, the ones specified in the database will be used.
           |
           |  java -jar om.jar --$COMMAND_INFO img --database-table om_cal:2004
           |
           | #---------------------------------------------------------------------------------------------------------
           | Example 5: Same as 5 but saving information into default output directory.
           |
           |  java -jar om.jar --$COMMAND_INFO_SAVE  img --database-table om_cal:2004
           |
           | #---------------------------------------------------------------------------------------------------------
           | Example 7: Syncronize a set of images in a remote host at directory 'dir' with a local directory,
           |            then calibrate them automatically. The syncronization set up is defined in the configuration
           |            file section 'RemoteSync'
           |            If 'db' and 'tb' are not specified, the ones specified in the database will be used.
           | java -jar om.jar --$COMMAND_REMOTE_SYNC_CAL 'dir' --database-table db:tn
           |
           | #---------------------------------------------------------------------------------------------------------
           | Example 8: Automatically detect in a remote host, the sequence of not calibrated directories created after
           |            '2021-03-17' and save them in 'oFile'
           |            The host is defined in the configuration file section 'RemoteSync'
           |   java -jar om.jar --$COMMAND_REMOTE_DIR_NOT_CAL '2021-03-17' --output-file oFile
           |
           | #---------------------------------------------------------------------------------------------------------
           | Example 8.1: Automatically detect in a remote host, the sequence of not calibrated directories created after
           |            current date minus 52 day and save them in 'oFile'
           |            The host is defined in the configuration file section 'RemoteSync'
           |   java -jar om.jar --$COMMAND_REMOTE_DIR_NOT_CAL 52 --output-file oFile
           |
           | #---------------------------------------------------------------------------------------------------------
           | Example 9: report the information of image 'f' but do not any other action ( e.g. add, calibrate...)
           |  .The report indicates the detected type of image, telescope,..., obtained parsing FITS header.
           |  .This option is used for debugging the FITS header of an image to be sure that relevant data is detected
           |  properly
           |  java -jar om.jar --$COMMAND_REPORT_IMAGE_INFO f
           | #---------------------------------------------------------------------------------------------------------
           |Options:
           |""".stripMargin)
  footer("\n'om' ends")

  val configurationFile = opt[String](
    default = Some("input/configuration/main.conf")
    ,  short = 'g'
    ,  descr = "om configuration file\n")

  val version = opt[String](required = false
    , noshort = true
    , descr = "Program version\n"
  )

  val outputImageDir = opt[String](default = Some("output/calibration/image_seq")
    ,  short = 'o'
    ,  descr = "default image output dir\n")

  val explore = opt[List[String]](
    short = 'x'
    , descr = "Explore all of directories (separated by spaces and surround by \") that includes the FITS images to process\n")

  val addCal = opt[List[String]](
     short = 'd'
    , descr = "List of directories (separated by spaces and surround by \") that includes the FITS images to process, parsed, stored into the database and calibrated\n")

  val excludeDirSeq = opt[List[String]](
    default = Some(List())
    , short = 'e'
    , descr = "List of directories (separated by spaces and surround by \") not to be processed. None by default\n")

  val showFitsRecord = opt[List[String]](
      short = 'h'
    , descr = "Show a sequence of FITS record from a FITS file stored in the main table (not in the calibration one). The first argument is the pattern for the record name to show (value 'all' for all records, 'allNotNull' for all records with not null value), second is the name of FITS file\n")

  val info = opt[String](
    short = 'i'
    ,  descr = s"Get all related image files used to perform the calibration of the a science image. Partial image name is allowed\n")

  val infoSave = opt[String](
    short = 'f'
    ,  descr = s"Save all related image files used to perform the calibration of the a science image and save them into default output directory. Partial image name is allowed\n")

  val addCalLabel = opt[String](required = false
    , short = 'l'
    , descr = "Input directory\n")

  val databaseTable = opt[String](
      required = false
    , short = 'y'
    , descr = "Database to be used\n")

  val remoteSyncCal = opt[String](required = false
    , short = 'r'
    , descr = "Remote synchronization and calibration\n")

  val remoteDirNotCal = opt[String](required = false
    , short = 'm'
    , descr = "Detect the sequence of directories not calibrated in a remote host\n")

  val outputFile = opt[String](required = false
    , default = Some("output/remote_file_to_sync")
    , short = 't'
    , descr = "output file\n")

  val reportImageInfo = opt[String](required = false
    , short = 'F'
    , descr = "Image file to be checked\n")

  val drop = opt[Boolean](required = false
    , descr = "Erase the main table and drop the calibration table\n")
  //-------------------------------------------------------------------------
  def isInvalidDirectory(seq: List[String]): Option[String] = {
    seq.foreach { f =>
      if (!directoryExist(f)) return Some(s"Invalid user argument 'includeDirSeq'. The directory '$f' does not exist")
    }
    None
  }
  //---------------------------------------------------------------------------
  validate (configurationFile) { f =>
    if (!fileExist(f) && (!directoryExist(f))) Left(s"Invalid user argument 'configurationFile'. The configuration file '$f' does not exist")
    else Right()
  }
  //---------------------------------------------------------------------------
  validate (excludeDirSeq) { seq =>
    val r = isInvalidDirectory(seq)
    if (r.isDefined) Left(r.get)
    else Right()
  }
  //---------------------------------------------------------------------------
  validate(outputImageDir) { _ =>
    Right()
  }
  //---------------------------------------------------------------------------
  validate (showFitsRecord) { seq =>
    if(seq.length < 2) Left(s"Invalid user argument 'showFitsRecord'. The waiting two arguments: record name to show and fits file")
    else Right()
  }
  //---------------------------------------------------------------------------
  validate (info) { _ => Right() }
  //---------------------------------------------------------------------------
  validate (infoSave) { _ => Right() }
  //---------------------------------------------------------------------------
  validate (addCalLabel) { p =>
    if (!directoryExist(p)) Left(s"Invalid user argument 'addCalLabel'. The path '$p' does not exist")
    else Right()
  }
  //---------------------------------------------------------------------------
  validate (addCalLabel) { p =>
    if (!directoryExist(p)) Left(s"Invalid user argument 'addCalLabel'. The path '$p' does not exist")
    else Right()
  }
  //---------------------------------------------------------------------------
  mutuallyExclusive(version, explore, addCal, addCalLabel, info, infoSave, remoteSyncCal, remoteDirNotCal, reportImageInfo)
  requireOne       (version, explore, addCal, addCalLabel, info, infoSave, remoteSyncCal, remoteDirNotCal, reportImageInfo)
  verify()
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CommandLineParser.scala
//=============================================================================