/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/Feb/2022
 * Time:  16h:33m
 * Description: None
 */
//=============================================================================
package com.om.sync
//=============================================================================
import com.common.configuration.MyConf
import com.common.logger.MyLogger
import com.common.util.mail.MailAgent
import com.common.util.path.Path
import com.common.util.util.Util
import com.om.calibration.{CalibratedFits, Calibration}
import com.om.{Main, ObservingManager}
import com.om.ObservingManager._
import com.om.TableDefinition.CALIBRATION_NAME
import com.om.calibration.Calibration.getCalibratedScienceImage
//=============================================================================
import java.time.LocalDate
import sys.process._
import scala.util.{Failure, Success, Try}
import java.io.{BufferedWriter, File, FileWriter}
import scala.collection.mutable.ArrayBuffer
import scala.language.postfixOps
//=============================================================================
//=============================================================================
object RemoteDirectorySync {
  //---------------------------------------------------------------------------
  private final val LOCAL_SOURCE_ROOT_DIR = Path.ensureEndWithFileSeparator(MyConf.c.getString("RemoteSync.localSourceRootDir"))
  private final val LOCAL_RESULT_ROOT_DIR = Path.ensureEndWithFileSeparator(MyConf.c.getString("RemoteSync.localResultDir"))
  //---------------------------------------------------------------------------
  private final val REMOTE_HOST                  = MyConf.c.getString("RemoteSync.host")
  private final val REMOTE_SOURCE_ROOT_DIR       = Path.ensureEndWithFileSeparator(MyConf.c.getString("RemoteSync.remoteSourceRootDir"))
  private final val REMOTE_COMMAND_FIND_TEMPLATE = s"ssh $REMOTE_HOST find DIR -type f -name '*.fit' -o -name '*.fts' -o -name '*.fits'"
  //---------------------------------------------------------------------------
  private final val ASTROMETRY_TMP_DIR     = Path.ensureEndWithFileSeparator(MyConf.c.getString("RemoteSync.astrometry.localTempDir"))
  private final val ASTROMETRY_SCRIPT_PATH = Path.ensureEndWithFileSeparator(MyConf.c.getString("RemoteSync.astrometry.scriptPath"))
  private final val ASTROMETRY_SCRIPT      = MyConf.c.getString("RemoteSync.astrometry.script")
  //---------------------------------------------------------------------------
  private final val M2_JAR_PATH                   = Path.ensureEndWithFileSeparator(MyConf.c.getString("RemoteSync.astrometry.m2.jarPath"))
  private final val M2_ASTROMETRY_CATALOG_COMMAND = MyConf.c.getString("RemoteSync.astrometry.m2.commandAstrometryCatalog")
  private final val M2_WCS_FITTING_COMMAND         = MyConf.c.getString("RemoteSync.astrometry.m2.commandWcsFitting")
  //---------------------------------------------------------------------------
  private final val REMOTE_RESULT_ROOT_DIR        = Path.ensureEndWithFileSeparator(MyConf.c.getString("RemoteSync.remoteResultBackupRootDir"))
  //---------------------------------------------------------------------------
  private final val mailAgent = MailAgent()
  private final val mailFrom = "rmorales@iaa.es"
  private final val mailTo = "rmorales@iaa.es"
  private final val mailCC = "nicolas@iaa.es"
  //---------------------------------------------------------------------------
}
//=============================================================================
import RemoteDirectorySync._
case class RemoteDirectorySync(databaseName: String, tableName: String) extends MyLogger {
  //---------------------------------------------------------------------------
  def run(remoteInputDir: String): Boolean = {
    if(remoteInputDir.isEmpty) return error("User's input dir is empty")

    val (r,localDir,resultDir) = copyRemoteImages(remoteInputDir)
    if (!r) return false
    val (r2,wcsDir,om) = calibrateDirAndSolveAstrometry(localDir,resultDir)
    if (!r2) return false

    //Creating a backup of the images calibrated
   // if (!syncronizeResultsWithRemoteRepo(Path.ensureEndWithFileSeparator(resultDir), year)) return false

    //remove temporal directories and send final mail
    val scienceImageList = Path.getSortedFileList(wcsDir) map (_.getName)
    Path.deleteDirectoryIfExist(ASTROMETRY_TMP_DIR)
    senMailSyncUpdated(remoteInputDir, scienceImageList, om)

    //wcs fitting
    val wcsFinalDir = resultDir + "/wcs/"
    info(s"WCS fitting of:'$wcsFinalDir'")
    if (!wcsFitting(wcsFinalDir)) return false

    //add images with WCS to astrometry database
    info(s"Adding images to astrometry database from dir:'$wcsFinalDir'")
    if (!addToAstrometryDB(wcsDir,wcsFinalDir)) return false

    true
  }
  //---------------------------------------------------------------------------
  def getNotCalibratedRemoteDirSeq(startDate: LocalDate, outputFile: String): Boolean = {
    val year = startDate.getYear
    val command = s"ssh $REMOTE_HOST find $REMOTE_SOURCE_ROOT_DIR$year -type f -name '*.fit' -o -name '*.fts' -o -name '*.fits' -newermt '${startDate.toString}'"
    warning(s"Executing command:'$command")
    Try {
      val remoteFileSeq  = (command!!).split("\n").filter(!_.isEmpty)
      if (remoteFileSeq.isEmpty) warning("No new files found in the remote path")
      else processRemoteFileSeq(remoteFileSeq, tableName, outputFile)
    }
    match {
      case Success(_) => true
      case Failure(ex) =>
        error(s"Error synchronizing with remote repository. Start date:'$startDate'" + ex.toString)
        false
    }
  }
  //----------------------------------------------------------------------------
  private def senMailSyncError(message:String
                                , dirName:String = ""
                                , subject:Option[String] = None) = {
    val mailContent = "Please contact with rmorales@iaa to trace the source of the error in the calibration." +
      s"\nError message:'$message'"+
      s"\nError message ends"
    mailAgent.send(mailTo
      , mailFrom
      , subject = subject.getOrElse(s"[om]: error processing directory: '$dirName'")
      , mailContent
      , mailCC)
    false
  }
  //----------------------------------------------------------------------------
  private def senMailSyncUpdated(remoteInputDir: String, scienceImageList: Seq[String], om: ObservingManager) = {

    val masterBiasSet = scala.collection.mutable.Set[String]()
    val masterDarkSet = scala.collection.mutable.Set[String]()
    val masterFlatSet = scala.collection.mutable.Set[String]()
    scienceImageList.map { imageName=>
      val r = om.getInfo(imageName,false)
        .split("\n")
        .drop(4)
        .take(3)

      masterBiasSet += r(0)
      masterDarkSet += r(1)
      masterFlatSet += r(2)
    }

    val masterSeq = masterBiasSet.mkString("\n") + "\n" +
      masterDarkSet.mkString("\n") + "\n" +
      masterFlatSet.mkString("\n") + "\n"
    val mailContent = masterSeq +
      "--- List of science images calibrated starts\n" +
     scienceImageList.mkString("\n") +
      "\n--- List of science images calibrated ends"
    mailAgent.send(mailTo
      , mailFrom
      , s"[om]: remote directory calibrated '$remoteInputDir' science image count:${scienceImageList.size}"
      , mailContent
      , mailCC)
    true
  }  //----------------------------------------------------------------------------
  private def senMailFindDirUpdated(dirList: Seq[String], filename: String) = {
    val mailContent = "--- List of uncalibrated directories starts\n" + dirList.sorted.mkString("\n") + "\n--- List of uncalibrated directories ends"
    mailAgent.send(mailTo
      , mailFrom
      , s"[om]: Found:${dirList.size} uncalibrated directories in dss16.iaa.es"
      , mailContent
      , mailCC
      , filename = Some(filename))
    true
  }
  //---------------------------------------------------------------------------
  private def wcsFitting(wcsDir: String): Boolean = {
    //check if directory is empty
    val isDirEmpty = new File(wcsDir).listFiles.filter(_.isFile).isEmpty
    if (isDirEmpty) {
      warning(s"Directory '$wcsDir' is empty. Can not add to fit wcs")
      return false
    }

    val command = s"${M2_WCS_FITTING_COMMAND.replaceAll("DIR_IN",wcsDir)} "
    info(s"command: '$command'")
    val path = M2_JAR_PATH
    Try { Process(command, new File(path)).!! }
    match {
      case Success(_) => true
      case Failure(ex) =>
        val message = s"Error fitting the WCS in directory:'$wcsDir'\n" + ex.toString
        error(message)
        senMailSyncError(message,wcsDir)
    }
  }
  //---------------------------------------------------------------------------
  private def addToAstrometryDB(wcsDir: String, resultDir: String): Boolean = {
    //check if directory is empty
    val isDirEmpty = new File(wcsDir).listFiles.filter(_.isFile).isEmpty
    if (isDirEmpty) {
      warning(s"Directory '$wcsDir' is empty. Can not add to astrometry database")
      return false
    }

    info(s"Adding new images to the m2 astrometry database")
    val command = s"$M2_ASTROMETRY_CATALOG_COMMAND  $wcsDir --root-path $resultDir"
    info(s"command: '$command'")
    val path = M2_JAR_PATH
    Try { Process(command, new File(path)).!! }
    match {
      case Success(_) => true
      case Failure(ex) =>
        val message = s"Error adding to astrometry database using m2 command in directory:'$wcsDir'\n" + ex.toString
        error(message)
        senMailSyncError(message,wcsDir)
    }
  }
  //---------------------------------------------------------------------------
  private def syncronizeResultsWithRemoteRepo(resultDir: String, year: String) = {
    info("Creating a backup of the images calibrated")
    val command = s"rsync -av $resultDir $REMOTE_RESULT_ROOT_DIR$year/"
    Try { command.!! }
    match {
      case Success(_) => true
      case Failure(ex) =>
        val message = s"Error creating a backup of the images calibrated\n" + ex.toString
        error(message)
        senMailSyncError(message,resultDir)
    }
  }
  //---------------------------------------------------------------------------
  private def forceDelete(oDir:String) = s"rm -fr $oDir"!
  //---------------------------------------------------------------------------
  private def solveAstrometry(imageSeq: Array[String], resultDir:String, localDir: String) : (Boolean,String) = {
    val oDir = s"${Path.getCurrentDirectory()}/$ASTROMETRY_TMP_DIR/"

    //reset directory. Do it manually because in there are links inside, the command 'Path.resetDirectory(oDir)' "fails"
    forceDelete(oDir)
    Path.createDirectoryIfNotExist(oDir)

    val wcsDir = oDir + "/wcs"
    val wcsNotSolved = wcsDir + "/not_solved"

    //create a temporal directory with a link to the calibrated images
    imageSeq.foreach { s=> s"ln -s $s $oDir"!!}

    info(s"Solving astrometry in all images of input directory: '$oDir' writing results in directory:'$wcsDir'")
    info(s"Astrometry script path: '$ASTROMETRY_SCRIPT_PATH'")

    Try {
      val command = s"cd $ASTROMETRY_SCRIPT_PATH && $ASTROMETRY_SCRIPT $oDir $wcsDir"
      info(s"Command: '$command'")
      val processBuilder = sys.process.Process(Seq("bash", "-c", command))
      processBuilder.!
    }
    match {
      case Success(_)  =>
        warning("Astrometry solver ends successfuly")
      case Failure(ex) =>
        val message = s"[om wcs error solving]\n" + ex.toString
        error(message)
        senMailSyncError(message,localDir)
        return (false,"")
    }
    //check the not solved images
    if (Path.directoryExist(wcsNotSolved)) {
      val notSolvedImageSeq = Path.getSortedFileList(wcsNotSolved, ".fits") map (_.getName)
      val notSolvedImageSeqLength  = notSolvedImageSeq.length
      if (notSolvedImageSeqLength > 0) {
        error(s"Not solved image dir: '$wcsNotSolved' with '$notSolvedImageSeqLength' images")
        senMailSyncError(s"\n---Not solved '$notSolvedImageSeqLength' images starts\n"
          + notSolvedImageSeq.mkString("\n")
          + s"\n---Not solved '$notSolvedImageSeqLength' images ends\n"
        , localDir
        , subject = Some(s"[om wcs error solving] in '$notSolvedImageSeqLength' images'"))
      }
    }
    //copy wcs to the result dir
    info("Copying wcs images")
    s"cp -r $wcsDir $resultDir".!!
    (true, wcsDir)
  }
  //---------------------------------------------------------------------------
  //(sucess,wcs_dir,om)
  def calibrateDirAndSolveAstrometry(localDir:String, resultDir:String): (Boolean, String, ObservingManager) = {
    info(s"Calibrating directory:'$localDir'")
    val r = Main.initMainObjects(null, resultDir, Some((databaseName,tableName)))
    val om = r._2
    val calTableName = om.tf.getQualifiedTableName(CALIBRATION_NAME)
    val lastCalRowID =
      if (!ObservingManager.existTable(databaseName,calTableName)) 0
      else om.getFirstOrLastRecordID(calTableName)

    //calibrate
    Try {
      om.exploreDir(
          List(localDir)
        , excludeDirSeq  = List())

      if (om.getFileCount > 0) om.calibrate(List(localDir))
    }
    match {
      case Success(_)  =>
        info(s"Directory:'$localDir' calibrated. Last row in the calibration table: '$calTableName' is: ${lastCalRowID.toString}")
        val calibratedScienceImageNameSeq = Calibration.getPath(getCalibratedScienceImage(calTableName,lastCalRowID.toString))
        if (calibratedScienceImageNameSeq.size > 0) {
          val (r, wcsDir) = solveAstrometry(calibratedScienceImageNameSeq.toArray, resultDir, localDir)
          (r, wcsDir, om)
        }
        else (false,"",null)

      case Failure(ex) =>
        val message = s"Error calibrating directory:'$localDir'\n" + ex.toString
        error(message)
        senMailSyncError(message, localDir)
        (false,"",null)
    }
  }
  //---------------------------------------------------------------------------
  //(result,newLocalDir,resultDir,year)
  private def copyRemoteImages(remoteDir: String): (Boolean,String,String) = {
    val splitSeq = remoteDir.split("/").filter(!_.isEmpty)
    val baseDir = Path.ensureEndWithFileSeparator(splitSeq.drop(3).mkString("/"))
    val localSourceDir = Path.ensureEndWithFileSeparator(s"$LOCAL_SOURCE_ROOT_DIR/$baseDir")
    val localResultDir = Path.ensureEndWithFileSeparator(s"${if (LOCAL_RESULT_ROOT_DIR.endsWith("/")) LOCAL_RESULT_ROOT_DIR.dropRight(1) else LOCAL_RESULT_ROOT_DIR}/om_cal_$tableName")

    Path.resetDirectory(localSourceDir)
    Path.ensureDirectoryExist(localResultDir)

    //get the list of remote images
    val remoteFindCommand = REMOTE_COMMAND_FIND_TEMPLATE.replace("DIR",remoteDir)
    var remoteCopyCommand = ""
    info(s"Executing remote command:'$remoteFindCommand'")
    val remoteImageSeq  = (remoteFindCommand!!).split("\n")

    Try { //copy remote files into local dir
      remoteImageSeq.foreach { remoteFile =>
        if (!CalibratedFits.isIgnoredPath(remoteFile)){
          val filenameNormalized = Path.getOnlyFilename(remoteFile).trim.replaceAll(" +", "_")
          remoteCopyCommand = s"scp -T $REMOTE_HOST:'$remoteFile' '$localSourceDir/$filenameNormalized'"
          info(s"Copying remote file:'$remoteFile'")
          Util.runShellWaitUntilTerminate(remoteCopyCommand)
        }
      }
    }
    match {
      case Success(_) => (true,localSourceDir,localResultDir)
      case Failure(ex) =>
        val message = s"Error copying remote file:'$remoteDir'\n" + ex.toString
        error(message)
        error (s"REmote command:$remoteCopyCommand")
        senMailSyncError(message, remoteDir)
        (false,"","")
    }
  }
  //---------------------------------------------------------------------------
  private def processRemoteFileSeq(remoteFileSeq: Seq[String], tableName: String, outputFile: String) : Boolean = {
    val totalRemoteFileCount = remoteFileSeq.size
    info(s"Processing $totalRemoteFileCount remote files")
    var newImageCount = 0
    var processedImageCount = 0
    val dirSet = scala.collection.mutable.Set[String]()
    val dirDetailedMap = scala.collection.mutable.Map[String,ArrayBuffer[String]]()
    val existTable = ObservingManager.existTable(databaseName,tableName)
    //------------------------------------------------------------------------
    def fileHasBeenProcessed (fileName: String) = {
      if (!existTable) false
      else getImageID(getExistByPathSQL(tableName, fileName)) != -1
    }
    //------------------------------------------------------------------------
    remoteFileSeq.zipWithIndex.foreach { case (path,i) =>
      info(s"Checking file: $i/$totalRemoteFileCount")
      val subStringToFind = path
        .trim
        .replaceAll(" +", "_")
        .split("/")
        .drop(3)
        .mkString("/")

      //filter if the path must be ignored or the image (bias, dark, flat or science) has been already processed by previously by om
      if (CalibratedFits.isIgnoredPath(path) || fileHasBeenProcessed(subStringToFind)) processedImageCount += 1
      else {
        newImageCount += 1
        val dir = Path.getParentPath(path)
        dirSet += dir
        if (dirDetailedMap.contains(dir)) dirDetailedMap(dir) += path
        else dirDetailedMap(dir) = ArrayBuffer(path)
      }
    }
    info(s"New remote image count       : $newImageCount")
    info(s"Already processed image count: $processedImageCount")
    if (dirSet.size > 0) {
      val dirSeq = dirSet.toArray.sorted
      info(s"Remote directories to sync:${dirSeq.length}\n\t" + dirSeq.mkString("\n\t"))

      //save dir file
      var bw = new BufferedWriter(new FileWriter(new File(outputFile)))
      dirSet.toArray.sorted.foreach(s=> bw.write(s + "\n"))
      bw.close()

      //save dir detailed
      val detailedFileName = outputFile+"_detailed.txt"
      bw = new BufferedWriter(new FileWriter(new File(detailedFileName)))
      dirDetailedMap.foreach { case (path, seq) =>
        bw.write(path + "\n")
        seq.sorted.foreach{ s=> bw.write("\t" + s + "\n") }
      }
      bw.close()

      //send mail
      senMailFindDirUpdated(dirSeq, detailedFileName)
    }
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file RemoteDirectorySync.scala
//=============================================================================
