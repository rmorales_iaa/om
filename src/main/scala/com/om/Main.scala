/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  31/Oct/2019
 * Time:  10h:33m
 * Description: observing manager
 */
//=============================================================================
package com.om
//=============================================================================
//=============================================================================
import BuildInfo.BuildInfo
import com.common.configuration.MyConf
import com.common.hardware.cpu.CPU
import com.common.logger.MyLogger
import com.common.util.path.Path
import com.common.util.time.Time
import com.common.util.time.Time.zoneID_UTC
import com.common.util.util.Util
import com.om.TableDefinition.CALIBRATION_NAME
import com.om.calibration.{CalibratedFits, Calibration}
import com.om.commandLine.CommandLineParser
import com.om.commandLine.CommandLineParser._
import com.om.sync.RemoteDirectorySync
//=============================================================================
import java.time.{Instant, LocalDate}
//=============================================================================
object Main extends MyLogger {
  //---------------------------------------------------------------------------
  //timing
  private var startMs: Long = 0
  //---------------------------------------------------------------------------
  private def initialActions(): Unit = {
    startMs = System.currentTimeMillis

    Instant.now.atZone(zoneID_UTC) // set time zone to UTC
  }
  //---------------------------------------------------------------------------
  private def finalActions(): Unit = {
    System.exit(0)
  }
  //---------------------------------------------------------------------------
  private def commandVersion(): Boolean = {
    println(Version.value)
    true
  }

  //---------------------------------------------------------------------------
  private def exploreRaw(includeDirSeq: List[String]
                         , excludeDirSeq: List[String]
                         , om: ObservingManager): Unit = {
    om.exploreDir(
      includeDirSeq
      , excludeDirSeq
      , checkIgnoredPath = true)
    info(s"Table used:'${om.tf.mainTableName}'")
  }
  //---------------------------------------------------------------------------
  private def getInitialID(om: ObservingManager, createTableDefinition: Boolean): Unit = {
    if (om.enableDatabase && !createTableDefinition) om.sessionFirstRowID = om.getFirstOrLastRecordID(isLast = false)
    if (om.enableDatabase) om.sessionLastRowID = om.getFirstOrLastRecordID(isLast = true) //get last id after parsing and adding to the main table
    om.calculateFileCount()
  }
  //---------------------------------------------------------------------------
  private def commandExplore(cl: CommandLineParser, om: ObservingManager): Unit = {
    info(s"\tCore count                       : '${CPU.getCoreCount()}'")
    exploreRaw(cl.explore()
               , cl.excludeDirSeq().map (d=> Path.ensureEndWithFileSeparator(d))
               , om)
  }
  //---------------------------------------------------------------------------
  private  def commandCalibrate(cl: CommandLineParser, om: ObservingManager): Unit = {

    info(s"\tCore count                       : '${CPU.getCoreCount()}'")

    if(cl.drop.isSupplied )
      info(s"\tDrop previous results:           : 'true'")
    else
      info(s"\tDrop previous results:           : 'false'")

    var createTableDefinition = false;
    if (cl.drop.isSupplied) {
      om.getDB().dropTable(om.tf.mainTableName)
      om.getDB().dropTable(om.tf.getQualifiedTableName(CALIBRATION_NAME))
      if (cl.outputImageDir.isSupplied)
        Path.resetDirectory(cl.outputImageDir())
      ensureDatabaseAndYearTableExists(om.tf.databaseName, om.tf.mainTableName)
    }
    exploreRaw(
      cl.addCal()
      , List()
      , om)

    getInitialID(om,createTableDefinition = false)
    if (om.getFileCount > 0) om.calibrate(cl.addCal())
  }

  //---------------------------------------------------------------------------
  private def commandInfo(cl: CommandLineParser, om: ObservingManager): Unit = {
    val partialName = cl.info()
    info(s"\tInfo of science calibration image: '$partialName'")
    info(s"\tCore count                       : '${CPU.getCoreCount()}'")
    println("#---------------------------------------")
    println(om.getInfo(partialName, false))
    println("#+++++++++++++++++++++++++++++++++++++++")
  }
  //---------------------------------------------------------------------------
  private def commandInfoSave(cl: CommandLineParser, om: ObservingManager): Unit = {
    val partialName = cl.infoSave()
    info(s"\tInfo of science calibration image: '$partialName' and save in default output directory")
    info(s"\tCore count                       : '${CPU.getCoreCount()}'")
    println("#---------------------------------------")
    println(om.getInfo(partialName, true))
    println("#+++++++++++++++++++++++++++++++++++++++")
  }
  //---------------------------------------------------------------------------
  private def commandAddCalibrationLabel(cl: CommandLineParser, om: ObservingManager): Unit = {
    info(s"Command:'$COMMAND_ADD_CAL_LABEL'")
    info(s"\tCore count                       : '${CPU.getCoreCount()}'")
    val inputDir = cl.addCalLabel()
    val outputDir = cl.outputImageDir()
    om.addCalibrationLabelToDir(inputDir, outputDir)
  }
  //---------------------------------------------------------------------------
  private def commandRemoteSyncCal(cl: CommandLineParser): Unit = {
    info(s"Command:'$COMMAND_REMOTE_SYNC_CAL'")
    info(s"\tCore count                       : '${CPU.getCoreCount()}'")
    val remoteInputDir = cl.remoteSyncCal()
    info(s"\tRemote synchronization directory: '$remoteInputDir'")
    val (databaseName,tableName) = getDatabaseAndTable(cl)
    RemoteDirectorySync(databaseName,tableName).run(remoteInputDir)
  }
  //---------------------------------------------------------------------------
  private def commandRemoteDirNotCal(cl: CommandLineParser): Unit = {
    info(s"Command:'$COMMAND_REMOTE_DIR_NOT_CAL'")
    val s = cl.remoteDirNotCal()
    val outputFileName = cl.outputFile()
    val startingDate =
      if (Util.isInteger(s)) {
        val now = LocalDate.now()
        val pastDate = LocalDate.now().minusDays(s.toInt)
        if (now.getYear > pastDate.getYear) LocalDate.parse(now.getYear + "-01-01")
        else pastDate
      }
      else {
        Time.parseLocalDate(s).getOrElse {
          error(s"User's input:'$s' is not a valid date")
          return
        }
      }
    info(s"Using starting date : '$startingDate'")
    info(s"Using output file name: '$outputFileName'")
    info(s"\tAutomatic detection of not calibrated directories in the remote host created after: ${startingDate.toString}'")
    val (databaseName,tableName) = getDatabaseAndTable(cl)
    RemoteDirectorySync(databaseName,tableName).getNotCalibratedRemoteDirSeq(startingDate, outputFileName)
  }
  //---------------------------------------------------------------------------
  private def commandReportImageInfo(cl: CommandLineParser): Unit = {
    info(s"Command:'$COMMAND_REPORT_IMAGE_INFO'")
    info(s"\tCore count                       : '${CPU.getCoreCount()}'")
    val fileName = cl.reportImageInfo()
    info(s"Image name: '$fileName'")
    CalibratedFits.reportImageInfo(fileName)
  }
  //---------------------------------------------------------------------------
  private def commandManager(cl: CommandLineParser
                             , om: ObservingManager
                             , tf: TableDefinition): Unit = {
    //common arguments
    info(s"\tConfigurationFile                : '${cl.configurationFile()}'")
    info(s"\tCore count                       : '${CPU.getCoreCount()}'")
    info(s"\tDatabase                         : '${tf.databaseName}'")
    info(s"\tMain table                       : '${tf.mainTableName}'")
    info(s"\tRemote database url              : '${MyConf.c.getString("Database.url")}'")
    info(s"\tDatabase user                    : '${MyConf.c.getString("Database.name")}'")
    info(s"\tAllowed FITS file extensions     : '${MyConf.c.getStringSeq("Util.fitsFileExtensionAllowed").mkString(", ")}'")
    if(cl.addCal.isDefined) {
      info(s"\tIncluding recursive directories  : '${cl.addCal().mkString(",")}'")
      info(s"\tExcluding recursive directories  : '${cl.excludeDirSeq().mkString(",")}'")
    }
    val command =
      if (cl.version.isDefined) commandVersion()
      else
        if (cl.addCal.isDefined) commandCalibrate(cl,om)
        else
          if (cl.addCalLabel.isDefined) commandAddCalibrationLabel(cl,om)
          else
            if (cl.explore.isDefined) commandExplore(cl,om)
            else
              if (cl.info.isDefined) commandInfo(cl,om)
              else
                if (cl.infoSave.isDefined) commandInfoSave(cl,om)
                else
                  if (cl.remoteSyncCal.isDefined)  commandRemoteSyncCal(cl)
                  else
                    if (cl.remoteDirNotCal.isDefined) commandRemoteDirNotCal(cl)
                    else
                      if (cl.reportImageInfo.isDefined) commandReportImageInfo(cl)
                      else "NONE"
  }
  //---------------------------------------------------------------------------
  private def getDatabaseAndTable(cl: CommandLineParser) = {

    if (cl !=null && cl.databaseTable.isDefined ) {
    val seq = cl.databaseTable().split(":")
    (seq(0),seq(1))
    }
    else
      (MyConf.c.getString("Database.database")
      , MyConf.c.getString("Database.mainTable"))
  }
  //---------------------------------------------------------------------------
  private def ensureDatabaseAndYearTableExists(databaseName: String
                                               , mainTableName: String) = {
    //be sure that database exists
    if (!Calibration.db.existDatabase(databaseName))
      Calibration.db.createDatabase(databaseName)

    if (!Calibration.db.existTable(databaseName,mainTableName)) {
        val yearDefinitionFile = MyConf.c.getString("Script.year_definition")
        val source = scala.io.Source.fromFile(yearDefinitionFile)
        val lines = try source.mkString finally source.close()
        val sql = lines.replaceAll("\\$YEAR",mainTableName)
        Calibration.db.createTable(sql)
    }
  }
  //---------------------------------------------------------------------------
  //init table definition and observing manager
  //(TableDefinition,ObservingManager)
  def initMainObjects(cl: CommandLineParser
                      , oDir: String
                      , userDatabaseAndTable: Option[(String,String)] = None) =  {
    val (databaseName,mainTableName) = userDatabaseAndTable.getOrElse(getDatabaseAndTable(cl))
    info(s"Using database  :'$databaseName'")
    info(s"Using main table:'$mainTableName'")
    val tf = TableDefinition(databaseName,mainTableName)
    val om =
      if (cl == null) ObservingManager(tf, oDir, enableDatabase = true, enableSSH_Connection = false)
      else ObservingManager(
        tf
      , oDir
      , enableSSH_Connection = cl.infoSave.isDefined)

    //assign global vars
    Calibration.db = om.getDB()
    Calibration.tf = tf
    Calibration.calibrationTableName = tf.getQualifiedTableName(CALIBRATION_NAME)

    ensureDatabaseAndYearTableExists(databaseName,mainTableName)

    (tf,om)
  }
  //---------------------------------------------------------------------------
  def main(args: Array[String]): Unit = {

    Sandbox()

//    initialActions()

    info(s"----------------------------- om '${BuildInfo.version}' starts -----------------------------")

    val userArgs = if (args.isEmpty) Array("--help") else args
    val cl = new CommandLineParser(userArgs)
    MyConf.c = MyConf(cl.configurationFile())
    if (MyConf.c == null || !MyConf.c.isLoaded )
      fatal(s"Error. Error parsing configuration file '${cl.configurationFile()}'")
    //-------------------------------------------------------------------------
    else {

      //sanitize output directory
      val oDir = Path.ensureEndWithFileSeparator(cl.outputImageDir())
      Path.ensureDirectoryExist(oDir)

      //init table definition and observing manager
      val (tf, om) = initMainObjects(cl, oDir)

      //run the user's command
      commandManager(cl,om,tf)

      om.close()
    }
    info(s"----------------------------- om '${BuildInfo.version}' ends -------------------------------")
    finalActions()
  }
}
//=============================================================================
//End of file Main.scala
//=============================================================================
