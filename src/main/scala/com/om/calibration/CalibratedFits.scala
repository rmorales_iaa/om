/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  09/Mar/2021
  * Time:  00h:35m
  * Description: None
  */
//=============================================================================
package com.om.calibration
//=============================================================================
import com.common.configuration.MyConf
import com.common.fits.simpleFits.SimpleFits
import com.common.fits.simpleFits.SimpleFits.{KEY_OBJECT, _}
import com.common.geometry.point.Point2D
import com.common.image.filter.FilterMatcher
import com.common.image.filter.FilterMatcher.{ALTERNATIVE_KEY_FILTER_SEQ, getAlternateFilterValue}
import com.common.image.instrument.{InstrumentMatcher, ZwoAsi_174}
import com.common.image.myImage.{ExposureTime, MyImage, ObservingTime}
import com.common.image.telescope.{Telescope, TelescopeMatcher}
import com.common.logger.{MyLogger, ThreadSafeLogger}
import com.common.util.path.Path
import com.common.util.time.Time
import com.common.util.time.Time.localDateTimeFormatterWithMillis
import com.common.util.util.Util
import com.om.TableDefinition.{BIAS_NAME, DARK_NAME, FLAT_NAME, SCIENCE_NAME}
import com.om.calibration.Calibration._
import com.om.calibration.ScienceCalibration.IGNORED_FILE_LOG
//=============================================================================
import java.sql.ResultSet
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatter._
import java.time.{LocalDate, LocalDateTime, LocalTime}
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.sys.process._
import scala.util.{Failure, Success, Try}
//=============================================================================
//=============================================================================
object CalibratedFits extends MyLogger {
  //---------------------------------------------------------------------------
  private var mainTableColNameSeq = updateMainTableColNameSeq()
  //---------------------------------------------------------------------------
  val map = scala.collection.mutable.Map[Int, CalibratedFits]()
  //---------------------------------------------------------------------------
  private final val ZWO_ASI_174_NAME = ZwoAsi_174().name
  //---------------------------------------------------------------------------
  //(xSize,ySize) -> (observingNightDate -> Seq(id))
  //1024x2048 ->
  //  11-02-21 -> Seq(1,5,6)
  //  11-02-22 -> Seq(8)
  val biasImagePartitionMap    = mutable.Map[Point2D, mutable.Map[LocalDate,ArrayBuffer[Int]]]()
  val darkImagePartitionMap    = mutable.Map[Point2D, mutable.Map[LocalDate,ArrayBuffer[Int]]]()
  val flatImagePartitionMap    = mutable.Map[Point2D, mutable.Map[LocalDate,ArrayBuffer[Int]]]()
  val scienceImagePartitionMap = mutable.Map[Point2D, mutable.Map[LocalDate,ArrayBuffer[Int]]]()
  //---------------------------------------------------------------------------
  var sessionFirstObservingNightDate : LocalDate = null
  var sessionLastObservingNightDate : LocalDate = null
  //---------------------------------------------------------------------------
  val FITS_MIN_PIX_ALLOWED = MyConf.c.getInt("FitsFilter.minPixSizeAllowed").toLong
  //---------------------------------------------------------------------------
  private final val OBSERVING_NIGHT_TIME_START = LocalTime.parse("12:00:00")
  //---------------------------------------------------------------------------
  final val ADDITIONAL_KEY_FILTER_SEQ =  Seq ( "EXTNAME" )
  //---------------------------------------------------------------------------
  final val PARSED_FITS_COL_SEQ =  Seq (
    "_id"
    , s"$KEY_BIT_PIX", s"$KEY_BSCALE", s"$KEY_BZERO", s"$KEY_NAXIS_1", s"$KEY_NAXIS_2"
    , s"$KEY_EXPTIME", s"$KEY_EXPOSURE", s"$KEY_EXP_TIME"
    , s"$KEY_DATEOBS", s"$KEY_TIMEOBS", s"$KEY_DATE", s"$KEY_TIME"
    , s"$KEY_UT", s"$KEY_UTSTART", s"$KEY_UTOBS"
    , s"$KEY_UTCSTART", s"$KEY_UTC_OBS", s"$KEY_UTCEND"
    , s"$KEY_FILTER"
    , s"$KEY_OBJECT", s"$KEY_OBJEC", s"$KEY_OBJCAT", s"$KEY_CAT_NAME", s"$KEY_CAT_NAME_1"
    , s"$KEY_IMAGETYP",s"$KEY_OBSTYPE",s"$KEY_OBS_TYPE", s"$KEY_OBS_TYPE_2"
    , s"$KEY_TELESCOPE", s"$KEY_OBSERVATORY", s"$KEY_ORIGIN"
    , s"$KEY_GCOUNT"
    , s"$KEY_INSTRUM"
    , s"$KEY_INSTRUME"
    , s"$KEY_EXTNAME"
    , s"$KEY_EXTVER"
    , s"$KEY_TEMP"
    , s"$KEY_CCD_TEMP"
    , s"$KEY_CCDTEMP"
    , s"$KEY_BINNING_1"
    , s"$KEY_BINNING_2"
    , s"$KEY_BINNING_3"
    , s"$KEY_BINNING_4"
    , s"$KEY_BINNING_5"
    , s"$KEY_BINNING_6"
    , s"$KEY_BINNING_7"
    , "_path") ++ ALTERNATIVE_KEY_FILTER_SEQ ++ ADDITIONAL_KEY_FILTER_SEQ
  //---------------------------------------------------------------------------
  private final val ignoredPathPattern =
    """.*basura.*|
      |.*borra.*|
      |.*_c\.f.*|
      |.*check.*|
      |.*combinado.*|
      |.*error.*|
      |.*focus.*|
      |.*foco.*|
      |.*frame.transfer.*|
      |.*frame_transfer.*|
      |.*master.*|
      |.*median.*|
      |.*moon.*|
      |.*super.*|
      |.*sample.*|
      |.*test.*|
      |.*trash.*|
      |.*hita.*|
      |.*ash.*|
      |.*sagra.*|
      |.*_kot.?_.*|
      |.*prueba.*|
      |.*grb.*|
      |.*apuntado.*|
      |.*pointing.*|
      |.*viñeteo.*|
      |.*guider.*|
      |.*lamdol.*|
      |.*lamdol.*|
      |.*landolt.*|
      |.*landholt.*|
      |.*standar.*|
      |.*temporal.*|
      |.*tmp.*|
      |.*fringing.*|
      |.*_ignore.*|
      |.*ignore.*|
      |.*om_ignore.*|
      |.*jupiter.*|
      |.*-neo|
      |neo|
      |.*moon.*|
      |.*luna.*|
      |.*oculta.*|
      |.*star.*|
      |.*ignore_cube.*|
      |.*occ.*""".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
  private final val frameTransferPattern =
    """.*frame.*transfer.*""".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
  private final val occultationPattern =
    """.*oculta.*|
      |.*occ.*""".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
  private final val invalidObjectPattern =
    """.*apuntado.*|
      |.*focus.*|
      |.*grb.*|
      |.*standar.*|
      |.*landol.*|
      |.*lamdol.*|
      |.*landholt.*|
      |.*jupiter.*|
      |.*jupyter.*|
      |.*calibration.*|
      |.*etalon.*|
      |.*test.*|
      |.*foco.*|
      |.*star.*|
      |.*pointing.*""".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
  private final val ignoreObjectTypePattern =
    """.*grb.*|
      |.*test.*|
      |.*standar.*|
      |.*landol.*|
      |.*lamdol.*|
      |.*landholt.*|
      |.*pointing.*|
      |.*focus.*|
      |.*apuntado.*|
      |.*foco""".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
  private final val ignoreImageTypePattern =
    """.*grb.*|
      |.*test.*|
      |.*standar.*|
      |.*landol.*|
      |.*lamdol.*|
      |.*landholt.*|
      |.*pointing.*|
      |.*focus.*|
      |.*apuntado.*|
      |.*foco.*""".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
  private def updateMainTableColNameSeq() =
    if(db != null) db.getColumnNameSeq() map (_.name) else ArrayBuffer[String]()
  //---------------------------------------------------------------------------
  private def getParsedFitsSQL() = {
    if(mainTableColNameSeq.isEmpty) mainTableColNameSeq = updateMainTableColNameSeq()
    val colSeq = PARSED_FITS_COL_SEQ.filter(mainTableColNameSeq.contains(_)).map(s=> "\"" + s + "\"").mkString(",")
    "SELECT " + colSeq + "\nFROM \""+ tf.mainTableName +"\""
  }
  //---------------------------------------------------------------------------
  //simple checking of number of extensions
  def isMultiExtensionFits(p: String) = {
    val r = s"astfits --numhdus  $p".!!.trim
    r != "1"
  }
  //---------------------------------------------------------------------------
  private def getTemperature(fits:SimpleFits): Option[Float] = {
    val temp  = fits.getStringValueOrEmptyNoQuotation(KEY_TEMP)
    val ccdTemp  = fits.getStringValueOrEmptyNoQuotation(KEY_CCD_TEMP)
    val ccdTemp2  = fits.getStringValueOrEmptyNoQuotation(KEY_CCDTEMP)
    if (Util.isFloat(temp))   return Some(temp.toFloat)
    if (Util.isFloat(ccdTemp)) return Some(ccdTemp.toFloat)
    if (Util.isFloat(ccdTemp2)) return Some(ccdTemp2.toFloat)
    None
  }
  //---------------------------------------------------------------------------
  def apply(path: String): Option[CalibratedFits] = {

    val fits = SimpleFits(path)

    val bitpix  = fits.getStringValueOrEmptyNoQuotation(KEY_BIT_PIX)
    val xPixSize= fits.getStringValueOrEmptyNoQuotation(KEY_NAXIS_1)
    val yPixSize= fits.getStringValueOrEmptyNoQuotation(KEY_NAXIS_2)

    var bScale  = fits.getStringValueOrEmptyNoQuotation(KEY_BSCALE)
    if (bScale.isEmpty) bScale = "1"

    var bZero   = fits.getStringValueOrEmptyNoQuotation(KEY_BZERO)
    if (bZero.isEmpty) bZero = "0"

    val expTime = fits.getStringValueOrEmptyNoQuotation(KEY_EXPTIME)
    val exposure= fits.getStringValueOrEmptyNoQuotation(KEY_EXPOSURE)
    val exp_time= fits.getStringValueOrEmptyNoQuotation(KEY_EXP_TIME)

    val dateObs = fits.getStringValueOrEmptyNoQuotation(KEY_DATEOBS)
    val timeObs = fits.getStringValueOrEmptyNoQuotation(KEY_TIMEOBS)
    val date    = fits.getStringValueOrEmptyNoQuotation(KEY_DATE)
    val time    = fits.getStringValueOrEmptyNoQuotation(KEY_TIME)

    val utStart = fits.getStringValueOrEmptyNoQuotation(KEY_UTSTART)
    val ut = fits.getStringValueOrEmptyNoQuotation(KEY_UT)
    val utObs = fits.getStringValueOrEmptyNoQuotation(KEY_UTOBS)

    val utcStart = fits.getStringValueOrEmptyNoQuotation(KEY_UTCSTART)
    val utcObs = fits.getStringValueOrEmptyNoQuotation(KEY_UTC_OBS)
    val utcEnd = fits.getStringValueOrEmptyNoQuotation(KEY_UTCEND)

    val filter  = fits.getStringValueOrEmptyNoQuotation(KEY_FILTER)
    val obj     = fits.getStringValueOrEmptyNoQuotation(KEY_OBJECT)
    val imgTyp  = fits.getStringValueOrEmptyNoQuotation(KEY_IMAGETYP)

    val obsTyp  = geObservingTypeRecord(fits)
    val telescope = TelescopeMatcher.getTelescope(fits)

    val timeStamp = ObservingTime.obtain(dateObs, timeObs, date, time, utStart, ut, utObs, utcStart, utcObs, utcEnd)
    val instrument = InstrumentMatcher.getInstrument(fits)
    val objectName = getObjectName(fits)
    val binning = getBinning(fits)
    val pixScale = getPixScale(telescope,instrument) * binning
    val fovX = (pixScale *  xPixSize.toDouble) / 60.0
    val fovY = (pixScale *  yPixSize.toDouble) / 60.0
    val flags  = ""

    if (isMultiExtensionFits(path)) {
      warning(s"FITS file '$path' is being ignored because it is a multi extension FITS")
      return None
    }
    val temperature = getTemperature(fits)
    val v = isValid(bitpix,xPixSize,yPixSize,bScale,bZero,path,expTime,exposure,exp_time,timeStamp,obj,imgTyp,obsTyp,temperature)
    if (!v.isDefined) return None

    val observingType = v.get._1
    val exposureTime = v.get._2
    val observingNightDate = v.get._3
    val alternateFilterValue = getAlternateFilterValue(fits)

    Some(CalibratedFits(
      observingType
      , observingNightDate
      , FilterMatcher.getObservingFilter(fits, filter, obj, alternateFilterValue, telescope, filter, path)
      , TelescopeMatcher.getTelescope(telescope, path)
      , bitpix.toInt
      , bScale.toFloat
      , bZero.toFloat.toInt
      , xPixSize.toInt
      , yPixSize.toInt
      , exposureTime
      , timeStamp.get
      , instrument
      , if(temperature.isDefined) temperature.get.toString else ""
      , objectName
      , binning
      , pixScale
      , pixScale
      , fovX
      , fovY
      , flags
      , path
      , outputImageDir))
  }
  //---------------------------------------------------------------------------
  def loadSessionFromMainTable(startID: Int, endID: Int) {

    //stats
    var totalImageCount = 0
    var biasImageCount = 0
    var darkImageCount = 0
    var flatImageCount = 0
    var scienceImageCount = 0
    var invalidIgnoredImageCount = 0

    //add range
    val idRangeSql =
    {
      warning(s"Processing all images with id from $startID to $endID (both included) of table '${Calibration.tf.mainTableName}'")
      s"""\nWHERE ( "_id" BETWEEN $startID AND $endID)"""
    }
    var rs: ResultSet = null
    val sql = getParsedFitsSQL + idRangeSql
    var currentImagePath = ""
    val observingNightSeq = ArrayBuffer[LocalDate]()
    rs = Calibration.db.select(sql)

    val colMap = Calibration.db.getColumnNameSeqAsMap(rs)
    //-----------------------------------------------------------------------
    def getColValueOrEmpty(colName: String, default: String = "") =
      if (colMap.contains(colName)) {
        val r = rs.getString(colName)
        if ((r == null) || r.isEmpty || r.toUpperCase == "NULL") default
        else r
      }
      else ""

    //-----------------------------------------------------------------------
    def getAlternateFilterValue(): String = {
      ALTERNATIVE_KEY_FILTER_SEQ.foreach { key =>
        val v = getColValueOrEmpty(key)
        if (!v.isEmpty) return v.trim.toLowerCase()
      }
      ""
    }
    //-----------------------------------------------------------------------
    while (rs.next) {
      totalImageCount += 1
      val path = rs.getString("_path")
      currentImagePath = path

      //first of all check path and multi extension fits
      var isIgnored = false
      if (isIgnoredPath(path)) {
        invalidIgnoredImageCount += 1
        currentImagePath = "NONE"
        isIgnored = true
        warning(s"FITS file '$path' match with the ignore path pattern")
      }
      else {
        Try {
          val id = rs.getInt("_id")
          val bitpix = getColValueOrEmpty(KEY_BIT_PIX)
          val xPixSize = getColValueOrEmpty(KEY_NAXIS_1)
          val yPixSize = getColValueOrEmpty(KEY_NAXIS_2)
          if (xPixSize.isEmpty || xPixSize.toInt == 0 ||
            yPixSize.isEmpty || yPixSize.toInt == 0)
            warning(s"FITS file '$path' has no data: xPixSize:'$xPixSize' yPixSize:'$yPixSize''")
          else {
            var bScale = getColValueOrEmpty(KEY_BSCALE, "1")
            if (bScale.isEmpty) bScale = "1"

            var bZero = getColValueOrEmpty(KEY_BZERO, "0")
            if (bZero.isEmpty) bZero = "0"

            val expTime = getColValueOrEmpty(KEY_EXPTIME)
            val exposure = getColValueOrEmpty(KEY_EXPOSURE)
            val exp_time = getColValueOrEmpty(KEY_EXP_TIME)

            val dateObs = getColValueOrEmpty(KEY_DATEOBS)
            val timeObs = getColValueOrEmpty(KEY_TIMEOBS)
            val date = getColValueOrEmpty(KEY_DATE)
            val time = getColValueOrEmpty(KEY_TIME)

            val utStart = getColValueOrEmpty(KEY_UTSTART)
            val ut = getColValueOrEmpty(KEY_UT)
            val utObs = getColValueOrEmpty(KEY_UTOBS)

            val utcStart = getColValueOrEmpty(KEY_UTCSTART)
            val utcObs = getColValueOrEmpty(KEY_UTC_OBS)
            val utcEnd = getColValueOrEmpty(KEY_UTCEND)

            val obj = getColValueOrEmpty(KEY_OBJECT)

            val imgTyp = getColValueOrEmpty(KEY_IMAGETYP)

            val obsTyp = geObservingTypeRecord(getColValueOrEmpty(KEY_OBSTYPE)
              , getColValueOrEmpty(KEY_OBS_TYPE)
              , getColValueOrEmpty(KEY_OBS_TYPE_2))

            val telescope = getTelescope(getColValueOrEmpty(KEY_TELESCOPE)
              , getColValueOrEmpty(KEY_OBSERVATORY)
              , getColValueOrEmpty(KEY_ORIGIN)
              , path)

            val timeStamp = ObservingTime.obtain(dateObs, timeObs, date, time, utStart, ut, utObs, utcStart, utcObs, utcEnd)
            val instrument = InstrumentMatcher.getInstrument(getColValueOrEmpty(KEY_INSTRUME)
                                                            , getColValueOrEmpty(KEY_INSTRUM)
                                                            , getColValueOrEmpty(KEY_EXTNAME)
                                                            , getColValueOrEmpty(KEY_EXTVER))

            val filter = FilterMatcher.getObservingFilter(
                getColValueOrEmpty(KEY_FILTER)
              , obj
              , getAlternateFilterValue()
              , path
              , telescope
              , instrument
              , FILTER_NUMBERED_NAME_SEQ.map(getColValueOrEmpty(_)))

            val objectSeq = Seq(getColValueOrEmpty(KEY_OBJECT)
              , getColValueOrEmpty(KEY_OBJEC)
              , getColValueOrEmpty(KEY_OBJCAT)
              , getColValueOrEmpty(KEY_CAT_NAME)
              , getColValueOrEmpty(KEY_CAT_NAME_1)
            ).filter(!_.isEmpty)
            val objectName = if (objectSeq.isEmpty) "None" else objectSeq.head
            val binningValueSeq = KEY_BINNING_SEQ.flatMap { s =>
              val r = getColValueOrEmpty(s)
              if (r.isEmpty) None
              else Some(r)
            }

            val binning = getBinning(binningValueSeq)
            val pixScale = getPixScale(telescope, instrument) * binning
            val fovX = (pixScale * xPixSize.toDouble) / 60.0
            val fovY = (pixScale * yPixSize.toDouble) / 60.0
            val flags = ""

            var temp = getColValueOrEmpty(KEY_TEMP)
            if (temp.isEmpty) temp = getColValueOrEmpty(KEY_CCD_TEMP)
            if (temp.isEmpty) temp = getColValueOrEmpty(KEY_CCDTEMP)
            val temperature = if (Util.isFloat(temp)) Some(temp.toFloat) else None

            val v = isValid(bitpix, xPixSize, yPixSize, bScale, bZero, path, expTime, exposure, exp_time, timeStamp, obj, imgTyp, obsTyp, temperature)
            if (v.isDefined && !isIgnored) {

              val observingType = v.get._1
              val observingNightDate = v.get._3
              val exposureTime = v.get._2

              //stats
              observingType match {
                case BIAS_NAME => biasImageCount += 1
                case DARK_NAME => darkImageCount += 1
                case FLAT_NAME => flatImageCount += 1
                case SCIENCE_NAME => scienceImageCount += 1
              }

              observingNightSeq += observingNightDate
              val pf = CalibratedFits(
                observingType
                , observingNightDate
                , filter
                , telescope
                , bitpix.toInt
                , bScale.toFloat
                , bZero.toFloat.toInt
                , xPixSize.toInt
                , yPixSize.toInt
                , exposureTime
                , timeStamp.get
                , instrument
                , if (temperature.isDefined) temperature.get.toString else ""
                , objectName
                , binning
                , pixScale
                , pixScale
                , fovX
                , fovY
                , flags
                , path
                , outputImageDir)

              pf.mainTableRelatedId = id
              addToStorage(pf)
            }
            else invalidIgnoredImageCount += 1
          }
          match {
            case Success(_) =>
            case Failure(_) => error(s"Error getting the FITS values of image: '$currentImagePath' .Ignoring it")
          }
        }
      }
     }
    rs.close

    //stats
    if (totalImageCount > 0) {
      val biasPercentage    = (biasImageCount.toFloat / totalImageCount) * 100
      val darkPercentage    = (darkImageCount.toFloat / totalImageCount) * 100
      val flatPercentage    = (flatImageCount.toFloat / totalImageCount) * 100
      val sciencePercentage = (scienceImageCount.toFloat / totalImageCount) * 100
      val invalidPercentage = (invalidIgnoredImageCount.toFloat / totalImageCount) * 100

      info(s"Total image count          : $totalImageCount")
      info(s"Valid BIAS image count     : ($biasImageCount)->${{f"$biasPercentage%.1f"}}%")
      info(s"Valid DARK image count     : ($darkImageCount)->${{f"$darkPercentage%.1f"}}%")
      info(s"Valid FLAT image count     : ($flatImageCount)->${{f"$flatPercentage%.1f"}}%")
      info(s"Valid SCIENCE image count  : ($scienceImageCount)->${{f"$sciencePercentage%.1f"}}%")
      info(s"Invalid/ignored image count: ($invalidIgnoredImageCount)->${{f"$invalidPercentage%.1f"}}%")
    }

    //get the first and last observing night
    val sortedSeq = observingNightSeq.sortWith(_.isBefore(_))
    if (!sortedSeq.isEmpty) {
      sessionFirstObservingNightDate = sortedSeq.head
      sessionLastObservingNightDate = sortedSeq.last
      info(s"Current session observing night from: '$sessionFirstObservingNightDate' to: '$sessionLastObservingNightDate' (both included)")
    }
    else info("No images parsed. Stopping")
  }
  //---------------------------------------------------------------------------
  def addToStorage(pf: CalibratedFits) = {
    //-------------------------------------------------------------------------
    val id = pf.mainTableRelatedId
    val dim = pf.getDim
    val observingNightDate = pf.observingNightDate
    //-------------------------------------------------------------------------
    def add(map: mutable.Map[Point2D,mutable.Map[LocalDate,ArrayBuffer[Int]]]) = {
      if (!map.contains(dim)) map(dim) = mutable.Map[LocalDate,ArrayBuffer[Int]]()
      val subMap = map(dim)
      if (!subMap.contains(observingNightDate)) subMap(observingNightDate) = ArrayBuffer[Int]()
      subMap(observingNightDate) += id
    }
    //-------------------------------------------------------------------------
    map(id) = pf
    //-------------------------------------------------------------------------
    pf.observingType match {
      case BIAS_NAME => add(biasImagePartitionMap)
      case DARK_NAME => add(darkImagePartitionMap)
      case FLAT_NAME => add(flatImagePartitionMap)
      case SCIENCE_NAME => add(scienceImagePartitionMap)
    }
  }

  //---------------------------------------------------------------------------
  def addToMasterStorage(pf: CalibratedFits) = {
    pf.observingType match {
      case BIAS_NAME => BiasCalibration.masterBiasSeq.add(pf)
      case DARK_NAME => DarkCalibration.masterDarkSeq.add(pf)
      case FLAT_NAME => FlatCalibration.masterFlatSeq.add(pf)
    }
  }
  //---------------------------------------------------------------------------
  //(observingType,exposureTime,observingNightDate)
  private def isValid(bitPix: String
                      , xPixSize : String
                      , yPixSize: String
                      , bScale : String
                      , bZero: String
                      , path:String
                      , expTime:String
                      , exposure:String
                      , exp_time:String
                      , localDateTime: Option[LocalDateTime]
                      , obj :String
                      , imgType : String
                      , obsType : String
                      , temperature: Option[Float]): Option[(String,Float,LocalDate)] = {

    //bitpix
    if (!Util.isInteger(bitPix)) {
      val message = s"has an unknown 'bitpix' value: '$bitPix'. Possible FITS format error"
      error(s"FITS file '$path' $message")
      ThreadSafeLogger(ScienceCalibration.IGNORED_FILE_LOG).add(path + "\t" + message)
      return None
    }

    //xPixSize
    if (!Util.isInteger(xPixSize)){
      val message = s"has an unknown 'NAXIS1' value : '$xPixSize'. Possible FITS format error"
      error(s"FITS file '$path' $message")
      ThreadSafeLogger(ScienceCalibration.IGNORED_FILE_LOG).add(path + "\t" + message)
      return None
    }

    //yPixSize
    if (!Util.isInteger(yPixSize)){
      val message = s"has an unknown 'NAXIS2' value: '$yPixSize'. Possible FITS format error"
      error(s"FITS file '$path' $message")
      ThreadSafeLogger(ScienceCalibration.IGNORED_FILE_LOG).add(path + "\t" + message)
      return None
    }

    //pixSize
    val totalPixSize = xPixSize.toLong * yPixSize.toLong
    if (totalPixSize <= FITS_MIN_PIX_ALLOWED) {
      val message = s"pix count: $totalPixSize (${xPixSize}x$yPixSize) has less pixels than the min allowed for an image: $FITS_MIN_PIX_ALLOWED"
      warning(s"FITS file '$path' with $message")
      ThreadSafeLogger(ScienceCalibration.IGNORED_FILE_LOG).add(path + "\t" + message)
      return None
    }

    //bScale
    if (!Util.isFloat(bScale)) {
      val message = s"has an unknown 'bScale' value: '$bScale'. Possible FITS format error"
      error(s"FITS file '$path' $message")
      ThreadSafeLogger(ScienceCalibration.IGNORED_FILE_LOG).add(path + "\t" + message)
      return None
    }

    //bZero
    if (!Util.isFloat(bZero)){
      error(s"FITS file '$path' has an unknown 'bZero' value: '$bZero'. Possible FITS format error")
      return None
    }

    //exposure time
    val exposureTime = ExposureTime.obtain(expTime,exposure,exp_time)
    if (exposureTime.isEmpty) {
      error(s"Error getting the exposure time on FITS file '$path'")
      ThreadSafeLogger(ScienceCalibration.IGNORED_FILE_LOG).add(path + "\t" + "error getting the exposure time")
      return None
    }

    //observing type
    val observingType = getObservingType(obj, imgType, obsType, exposureTime.get, path, temperature)
    if (observingType.isEmpty) return None

    //localDateTime
    if (localDateTime.isEmpty) {
      error(s"Error can not find a valid timestamp in FITS file '$path'")
      ThreadSafeLogger(ScienceCalibration.IGNORED_FILE_LOG).add(path + "\t" + "can not find a valid timestamp")
      return None
    }

    Some((observingType.get,exposureTime.get,getObservingNightDate(localDateTime.get)))
  }
  //---------------------------------------------------------------------------
  private def getPixScale(telescope: String, instrument: String) = {
    if (telescope == Telescope.UNKNOWN_TELESCOPE) -1d
    else {
      val telescopeName = Telescope.getNormalizedTelescopeName(telescope).toUpperCase
      Telescope.getPixScale(telescopeName, instrument)
    }
  }
  //---------------------------------------------------------------------------
  def getObservingInfo(path: String) = {
    val img     = MyImage(path)
    val fits    = img.getSimpleFits
    val obj     = fits.getStringValueOrEmptyNoQuotation(KEY_OBJECT)
    val imgType = fits.getStringValueOrEmptyNoQuotation(KEY_IMAGETYP)
    val obsType = geObservingTypeRecord(fits)

    val expTime = fits.getStringValueOrEmptyNoQuotation(KEY_EXPTIME)
    val exposure = fits.getStringValueOrEmptyNoQuotation(KEY_EXPOSURE)
    val exp_time = fits.getStringValueOrEmptyNoQuotation(KEY_EXP_TIME)
    val exposureTime = ExposureTime.obtain(expTime,exposure,exp_time)

    val dateObs = fits.getStringValueOrEmptyNoQuotation(KEY_DATEOBS)
    val timeObs = fits.getStringValueOrEmptyNoQuotation(KEY_TIMEOBS)
    val time = fits.getStringValueOrEmptyNoQuotation(KEY_TIME)
    val date = fits.getStringValueOrEmptyNoQuotation(KEY_DATE)

    val utStart = fits.getStringValueOrEmptyNoQuotation(KEY_UTSTART)
    val ut = fits.getStringValueOrEmptyNoQuotation(KEY_UT)
    val utObs = fits.getStringValueOrEmptyNoQuotation(KEY_UTOBS)

    val utcStart = fits.getStringValueOrEmptyNoQuotation(KEY_UTCSTART)
    val utcObs = fits.getStringValueOrEmptyNoQuotation(KEY_UTC_OBS)
    val utcEnd = fits.getStringValueOrEmptyNoQuotation(KEY_UTCEND)
    val observingTimeStamp = ObservingTime.obtain(dateObs, timeObs, date, time, utStart, ut, utObs, utcStart, utcObs, utcEnd)

    var telescope = TelescopeMatcher.getTelescope(fits)
    val instrument = InstrumentMatcher.getInstrument(fits)
    val filter = FilterMatcher.getObservingFilter(fits,telescope,instrument)

    val temperature = getTemperature(fits)

    val observingType = getObservingType(obj, imgType, obsType, exposureTime.get, path, temperature)
    val flags = fits.getStringValueOrEmptyNoQuotation(KEY_OM_FLAGS)

    val objectName = getObjectName(fits)
    val binning = getBinning(fits)
    val pixScale = getPixScale(telescope,instrument) * binning
    val fovX = (pixScale *  img.xMax.toDouble) / 60.0  //in arc min
    val fovY = (pixScale *  img.yMax.toDouble) / 60.0  //in arc min

    if (instrument == ZWO_ASI_174_NAME)
      telescope = "La_Sagra"

   ( observingType     //1
     , telescope       //2
     , filter          //3
     , exposureTime    //4
     , observingTimeStamp //5
     , flags           //6
     , instrument      //7
     , temperature     //8
     , objectName      //9
     , binning         //10
     , pixScale        //11
     , fovX            //12
     , fovY            //13
   )
  }
  //---------------------------------------------------------------------------
  def getObservingType(obj: String
                       , imgType: String
                       , obsType: String
                       , expTime: Float
                       , path: String
                       , temperature: Option[Float]) : Option[String] = {

    if (ignoreImageType(imgType)){
      warning(s"Image: '$path' has matched with the ignored image type pattern")
      return None
    }

    if (ignoreObjectType(obsType)){
      warning(s"Image: '$path' with type:'$obsType' has matched with the ignored object type pattern")
      return None
    }

    if (isNotValidObject(obj)) {
      warning(s"Image: '$path' with object:'$obj' has matched with the invalid object pattern")
      return None
    }

    if (isOccultation(imgType) || isOccultation(obsType)) {
      warning(s"Image: '$path' is an occultation. Ignoring it")
      return None
    }
    if (isFrameTransfer(imgType) || isFrameTransfer(obsType)){
      warning(s"Image: '$path' is an frame transfer. Ignoring it")
      return None
    }

    if (DarkCalibration.isDark(obj, imgType, obsType, expTime, path, temperature)) return Some(DARK_NAME)
    if (BiasCalibration.isBias(obj, imgType, obsType, expTime, path)) return Some(BIAS_NAME)
    if (FlatCalibration.isFlat(obj, imgType, obsType, expTime, path)) return Some(FLAT_NAME)
    if (ScienceCalibration.isScience(obj, imgType, expTime, path)) return Some(SCIENCE_NAME)

    val message = "is not a valid BIAS, DARK, FLAT or SCIENCE image"
    warning(s"Image: '$path' $message")
    ThreadSafeLogger(IGNORED_FILE_LOG).add(path + "\t" + message)
    None
  }
  //---------------------------------------------------------------------------
  def geObservingTypeRecord(fits: SimpleFits) : String = {
    val a = fits.getStringValueOrEmptyNoQuotation(KEY_OBSTYPE).replaceAll("'","").trim
    if (!a.isEmpty) return a

    val b = fits.getStringValueOrEmptyNoQuotation(KEY_OBS_TYPE).replaceAll("'","").trim
    if (!b.isEmpty) return b

    val c = fits.getStringValueOrEmptyNoQuotation(KEY_OBS_TYPE_2).replaceAll("'","").trim
    if (!c.isEmpty) return c
    ""
  }
  //---------------------------------------------------------------------------
  def geObservingTypeRecord(a: String, b: String, c: String) : String = {
    if (!a.isEmpty) return a
    if (!b.isEmpty) return b
    if (!c.isEmpty) return c
    c
  }

  //---------------------------------------------------------------------------
  def getTelescope(a: String
                   , b: String
                   , c: String
                   , path: String): String = {

    var r = TelescopeMatcher.getTelescope(a, path)
    if (!r.isEmpty && r != Telescope.UNKNOWN_TELESCOPE) return r

    r = TelescopeMatcher.getTelescope(b, path)
    if (!r.isEmpty && r != Telescope.UNKNOWN_TELESCOPE) return r

    r = TelescopeMatcher.getTelescope(c, path)
    if (!r.isEmpty && r != Telescope.UNKNOWN_TELESCOPE) return r

    Telescope.UNKNOWN_TELESCOPE
  }
  //---------------------------------------------------------------------------
  def getObservingNightDate(localDateTime: LocalDateTime) = {
    val currentNightDate = localDateTime.toLocalDate
    val currentLocalTime = localDateTime.toLocalTime
    if (currentLocalTime.isAfter(OBSERVING_NIGHT_TIME_START)) currentNightDate
    else currentNightDate.minusDays(1)
  }
  //---------------------------------------------------------------------------
  private def getObjectName(fits: SimpleFits): String = {
    val r = Seq(KEY_OBJECT, KEY_OBJEC, KEY_OBJCAT, KEY_CAT_NAME, KEY_CAT_NAME_1) flatMap { s=>
      val r = fits.getStringValueOrEmptyNoQuotation(s)
      if (r.isEmpty) None
      else Some(r)
    }

    if (r.isEmpty) "UNKNOWN"
    else r.head
  }
  //---------------------------------------------------------------------------
  private def getBinning(fits: SimpleFits): Int = {
    val r = KEY_BINNING_SEQ.flatMap{ s=>
      val r = fits.getStringValueOrEmptyNoQuotation(s)
      if (r.isEmpty) None
      else Some(r)
    }
    if (r.isEmpty) 1
    else getBinning(r)
  }
  //---------------------------------------------------------------------------
  private def getBinning(seq: Seq[String]): Int = {
    if (seq.isEmpty) 1
    else {
      if (Util.isInteger(seq.head)) {
        val v = seq.head.toInt
        if (v == 0) 1 else v   //at least binning 0
      }
      else {
        if (!seq.head.contains("x")) 1
        else seq.head.toLowerCase().split("x").head.toInt
      }
    }
  }
  //---------------------------------------------------------------------------
  def isIgnoredPath(path: String) =
    path.toLowerCase.trim match {
      case ignoredPathPattern(_*) => true
      case _ => false
    }

  //---------------------------------------------------------------------------
  def isFrameTransfer(s: String) =
    s.toLowerCase.trim match {
      case frameTransferPattern(_*) => true
      case _ => false
    }
  //---------------------------------------------------------------------------
  def isOccultation(s: String) =
    s.toLowerCase.trim match {
      case occultationPattern(_*) => true
      case _ => false
    }

  //---------------------------------------------------------------------------
  def isNotValidObject(s: String) =
    s.toLowerCase.trim match {
      case invalidObjectPattern(_*) => true
      case _ => false
    }
  //---------------------------------------------------------------------------
  def ignoreObjectType(s: String) =
    s.toLowerCase.trim match {
      case ignoreObjectTypePattern(_*) => true
      case _ => false
    }
  //---------------------------------------------------------------------------
  def ignoreImageType(s: String) =
    s.toLowerCase.trim match {
      case ignoreImageTypePattern(_*) => true
      case _ => false
    }
  //---------------------------------------------------------------------------
  def loadFromDatabase(sql: String, keepName: Boolean = false) = {
    val  parsedFitsSeq= ArrayBuffer[CalibratedFits]()
    var rs : ResultSet = null
    Try {
      rs = db.select(sql)
      while(rs.next) {

        val pf = CalibratedFits(
          rs.getString("observingType")
          , rs.getDate("observingNightDate").toLocalDate
          , rs.getString("filter")
          , rs.getString("telescope")
          , rs.getInt("bitPix")
          , rs.getDouble("bScale")
          , rs.getInt("bZero")
          , rs.getInt("xPixSize")
          , rs.getInt("yPixSize")
          , rs.getFloat("exposureTime")
          , rs.getTimestamp("observingTimeStamp").toLocalDateTime
          , rs.getString("instrument")
          , rs.getString("temperature")
          , rs.getString("object")
          , rs.getInt("binning")
          , rs.getFloat("pixScaleX")
          , rs.getFloat("pixScaleY")
          , rs.getFloat("fovX")
          , rs.getFloat("fovY")
          , rs.getString("flags")
          , rs.getString("_path")
          , outputImageDir
          , keepName)

        if(pf.observingType == SCIENCE_NAME) pf.mainTableRelatedId = getImageID(pf.path)
        else pf.calibrationTableId = rs.getInt("_id")

        pf.relatedBiasId = rs.getString("relatedBiasId")
        pf.relatedDarkId = rs.getString("relatedDarkId")
        pf.relatedFlatId = rs.getString("relatedFlatId")
        pf.sourceImageIdSeq = rs.getString("sourceImageIdSeq")

        parsedFitsSeq += pf
      }
    }
    match {
      case Success(_) => rs.close
      case Failure(e) => rs.close
        error("Method: 'Calibration.userProcessSingleItem' " + e.getMessage + " " + e.getStackTrace.mkString("\n"))
        error(e.getStackTrace.mkString("\n"))
        error(e.getMessage + s". Error processing results of sql '$sql' in PostgreDB")
    }
    parsedFitsSeq
  }
  //---------------------------------------------------------------------------
  def groupByTelescope(pfSeq: Array[CalibratedFits]) = {
    val map = scala.collection.mutable.Map[String, ArrayBuffer[CalibratedFits]]()
    pfSeq foreach { pf =>
      val telescope = pf.telescope
      if (!map.contains(telescope)) map(telescope) = ArrayBuffer[CalibratedFits]()
      map(telescope).append(pf)
    }
    map
  }
  //---------------------------------------------------------------------------
  def groupByInstrument(pfSeq: Array[CalibratedFits]) = {
    val map = scala.collection.mutable.Map[String, ArrayBuffer[CalibratedFits]]()
    pfSeq foreach { pf =>
      val instrument = pf.instrument
      if (!map.contains(instrument)) map(instrument) = ArrayBuffer[CalibratedFits]()
      map(instrument).append(pf)
    }
    map
  }
  //---------------------------------------------------------------------------
  def groupByFilter(pfSeq: Array[CalibratedFits]) = {
    val map = scala.collection.mutable.Map[String, ArrayBuffer[CalibratedFits]]()
    pfSeq foreach { pf =>
      val filter = pf.filter
      if (!map.contains(filter)) map(filter) = ArrayBuffer[CalibratedFits]()
      map(filter).append(pf)
    }
    map
  }
  //---------------------------------------------------------------------------
  def groupByExposureTime(pfSeq: Array[CalibratedFits]) = {
    val map = scala.collection.mutable.Map[Float, ArrayBuffer[CalibratedFits]]()
    pfSeq foreach { pf =>
      val exposureTime = pf.exposureTime
      if (!map.contains(exposureTime)) map(exposureTime) = ArrayBuffer[CalibratedFits]()
      map(exposureTime).append(pf)
    }
    map
  }
  //---------------------------------------------------------------------------
  def groupByExposureTime(pfSeq: Array[CalibratedFits], margin:Double) = {
    val map = scala.collection.mutable.Map[(Double,Double), ArrayBuffer[CalibratedFits]]()
    pfSeq foreach { pf =>
      val exposureTime = pf.exposureTime

      var stored = false
      map.foreach { case ((minExpTime,maxExpTime),storage) =>
        if (exposureTime >= minExpTime &&
            exposureTime <= maxExpTime) {
          storage.append(pf)
          stored = true
        }
      }
      if (!stored)
        map((exposureTime - margin, exposureTime + margin)) = ArrayBuffer[CalibratedFits](pf)
    }
    map
  }

  //---------------------------------------------------------------------------
  def groupByTemperature(pfSeq: Array[CalibratedFits], margin: Double) = {
    val map = scala.collection.mutable.Map[(Double, Double), ArrayBuffer[CalibratedFits]]()
    pfSeq foreach { pf =>
      val temperature = pf.temperature.toDouble

      var stored = false
      map.foreach { case ((minTemp, maxTemp), storage) =>
        if (temperature >= minTemp  &&
          temperature <= maxTemp) {
          storage.append(pf)
          stored = true
        }
      }
      if (!stored)
        map((temperature - margin, temperature + margin)) = ArrayBuffer[CalibratedFits](pf)
    }
    map
  }
  //---------------------------------------------------------------------------
  def reportImageInfo(fileName: String) ={
    val onlyName = Path.getOnlyFilenameNoExtension(fileName)


    val oi = getObservingInfo(fileName)

    println(s"--- '$onlyName' starts ---")
    info(s"Observing type: ${oi._1}")
    info(s"Telescope     : ${oi._2}")
    info(s"Filter        : ${oi._3}")
    info(s"Exposure time : ${oi._4}")
    info(s"Observing Time: ${oi._5}")
    info(s"Flags         : ${oi._6}")
    info(s"Instrument    : ${oi._7}")
    info(s"Temperature   : ${oi._8}")
    info(s"Object        : ${oi._9}")
    info(s"Binning       : ${oi._10}")
    info(s"PixScale      : ${oi._11}")
    info(s"FovX          : ${oi._12}")
    info(s"FovY          : ${oi._13}")
    if (isMultiExtensionFits(fileName)) info("Multi extension FITS!!!!")
    println(s"--- '$onlyName' ends ---")
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class CalibratedFits(observingType: String
                          , observingNightDate: LocalDate
                          , filter: String
                          , telescope: String
                          , bitPix: Int
                          , bScale: Double
                          , bZero: Int
                          , xPixSize: Int
                          , yPixSize: Int
                          , exposureTime: Float
                          , observingTimeStamp: LocalDateTime
                          , instrument: String
                          , temperature: String
                          , objectName: String
                          , binning: Int
                          , pixScaleX: Double
                          , pixScaleY: Double
                          , fovX: Double
                          , fovY: Double
                          , flags: String
                          , path:String
                          , outputImageDir: String
                          , keepName: Boolean = false) {
  //---------------------------------------------------------------------------
  var mainTableRelatedId: Int = -1
  private var calibrationTableId: Int = -1
  var relatedBiasId: String = "NULL"
  var relatedDarkId: String = "NULL"
  var relatedFlatId: String = "NULL"
  var sourceImageIdSeq: String = ""
  private val itemDivider = "#"
  //---------------------------------------------------------------------------
  val masterFileName =
    if(keepName) path
    else
      (getRootDir() +
        observingType + Path.FileSeparator +
        observingType + itemDivider + (if (observingType != SCIENCE_NAME) "master_" else "") +
        s"${Time.getTimeStampWindowsCompatible(observingTimeStamp.format(localDateTimeFormatterWithMillis))}" + itemDivider +
        s"${xPixSize}x$yPixSize" + itemDivider +
        s"$filter" + itemDivider +
        s"$telescope" + itemDivider +
        s"${Math.round(exposureTime)}s"+ itemDivider + Path.getOnlyFilenameNoExtension(path).replaceAll("/", "_").replaceAll(":", "_") + itemDivider +
        s"$instrument" + itemDivider +
        s"$objectName")
        .replaceAll(" ","_")
        .replaceAll("__","_")
        .replaceAll("%","_")
        .replaceAll("\\(","_")
        .replaceAll("\\)","_")
        .replaceAll("\\.","_") +
        ".fits"
  //---------------------------------------------------------------------------
  private def getRootDir() : String = if (outputImageDir == null) "output/" else Path.ensureEndWithFileSeparator(outputImageDir)
  //---------------------------------------------------------------------------
  def getDim = Point2D(xPixSize,yPixSize)
  //---------------------------------------------------------------------------
  def canMatch(other: CalibratedFits)= {
      telescope  == other.telescope &&
      instrument == other.instrument &&
      bitPix     == other.bitPix &&
      bScale     == other.bScale &&
      bZero      == other.bZero &&
      xPixSize   == other.xPixSize &&
      yPixSize   == other.yPixSize
  }
  //---------------------------------------------------------------------------
  def getBasicFitsHeader(obsType: String) =
    ArrayBuffer(
      (KEY_OM_MASTER, s"'$obsType'")
      , (KEY_OM_DATEOBS, s"'${observingTimeStamp.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)}'")
      , (KEY_OM_EXPTIME, s"'$exposureTime'")
      , (KEY_OM_FILTER, s"'$filter'")
      , (KEY_OM_TELESCOPE, s"'$telescope'")
      , (KEY_OM_INSTRUMENT, s"'$instrument'")
      , (KEY_OM_TEMPERATURE, s"'$temperature'")
      , (KEY_OM_OBJECT, s"'$objectName'")
      , (KEY_OM_BINNING, s"$binning")
      , (KEY_OM_PIX_SCALE_X, s"${if (pixScaleX != -1) pixScaleX else ""}")
      , (KEY_OM_PIX_SCALE_Y, s"${if (pixScaleY != -1) pixScaleY else ""}")
      , (KEY_OM_FOV_X, s"${if (fovX != -1) f"$fovX%.3f" else ""}")
      , (KEY_OM_FOV_Y, s"${if (fovY != -1) f"$fovY%.3f" else ""}")
    )
  //---------------------------------------------------------------------------
  def getValueSeq() =
    Seq(
      bitPix.toString
      , bScale.toString
      , bZero.toString
      , xPixSize.toString
      , yPixSize.toString
      , observingType
      , observingNightDate.toString
      , telescope
      , filter
      , instrument
      , temperature
      , objectName
      , exposureTime.toString
      , observingTimeStamp.format(ISO_LOCAL_DATE_TIME)
      , binning.toString
      , f"$pixScaleX%.3f"
      , f"$pixScaleY%.3f"
      , f"$fovX%.3f"
      , f"$fovY%.3f"
      , flags
      , masterFileName
      , relatedBiasId
      , relatedDarkId
      , relatedFlatId
      , sourceImageIdSeq
    )
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Calibrated.scala
//=============================================================================
