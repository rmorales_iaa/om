/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  16/Feb/2021
  * Time:  18h:12m
  * Description: None
  */
//=============================================================================
package com.om.calibration
//=============================================================================
import java.net.InetAddress
import java.sql.ResultSet
import java.time.LocalDate
import java.time.temporal.ChronoUnit
import java.util.concurrent.ConcurrentLinkedQueue
import scala.collection.mutable.ArrayBuffer
import scala.util.{Failure, Success, Try}
//=============================================================================
import com.om.ObservingType.{BIAS, FLAT, SCIENCE}
import com.om.{TableDefinition}
import com.common.configuration.MyConf
import com.common.database.postgreDB.PostgreRawDB
import com.common.logger.MyLogger
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
import com.om.ObservingType.{OBSERVING_TYPE, _}
import com.common.math.MyMath
import com.common.util.time.Time
import com.om.ObservingManager
import com.om.TableDefinition._
import com.om.ObservingManager.getImageId_ByPathSQL
import com.common.hardware.cpu.CPU
//=============================================================================
//=============================================================================
object Calibration extends MyLogger {
  //---------------------------------------------------------------------------
  var db: PostgreRawDB = null
  var tf: TableDefinition  = null
  var outputImageDir : String = null
  val user = MyConf.c.getString("Database.name")
  val localhost = InetAddress.getLocalHost.toString
  var calibrationTableName = ""
  var calibrateImageCount : Long = 0
  private var calibrateImageTotalCount : Long = 0
  //---------------------------------------------------------------------------
  final val CSV_FIELD_DIVIDER   = "\t"
  //---------------------------------------------------------------------------
  private var calibrationTableColumnName = Array("")
  //---------------------------------------------------------------------------
  private val maxDayDifferenceForMatching = MyConf.c.getInt("Calibration.maxDayDifferenceAllowedForMatching")
  //---------------------------------------------------------------------------
  private def tableDefinition(tableName: String) = {
    s"""CREATE TABLE "$tableName" (
       |   "_id" SERIAL PRIMARY KEY NOT NULL
       |  , "_timestamp" timestamp NOT NULL
       |  , "_ip" varchar(256) NOT NULL
       |  , "_user" varchar(256) NOT NULL
       |  , "bitPix" integer NOT NULL
       |  , "bScale" float NOT NULL
       |  , "bZero" integer NOT NULL
       |  , "xPixSize" integer NOT NULL
       |  , "yPixSize" integer NOT NULL
       |  , "observingType" varchar(64) NOT NULL
       |  , "observingNightDate" date NOT NULL
       |  , "telescope" varchar(64) NOT NULL
       |  , "filter" varchar(64) NOT NULL
       |  , "instrument" varchar(64)
       |  , "temperature" varchar(64)
       |  , "object" varchar(64)
       |  , "exposureTime" real NOT NULL
       |  , "observingTimeStamp" TIMESTAMP NOT NULL
       |  , "binning" integer NOT NULL
       |  , "pixScaleX" real NOT NULL
       |  , "pixScaleY" real NOT NULL
       |  , "fovX" real NOT NULL
       |  , "fovY" real NOT NULL
       |  , "flags" varchar(80)
       |  , "_path" varchar(1024) NOT NULL
       |  , "relatedBiasId" INTEGER NULL
       |  , "relatedDarkId" INTEGER NULL
       |  , "relatedFlatId" INTEGER NULL
       |  , "sourceImageIdSeq" varchar(2048) NOT NULL
       |
       |  , CONSTRAINT "fk_relatedBiasId"
       |      FOREIGN KEY("relatedBiasId")
       |      REFERENCES "$tableName"("_id")
       |
       |  , CONSTRAINT "fk_relatedDarkId"
       |      FOREIGN KEY("relatedDarkId")
       |      REFERENCES "$tableName"("_id")
       |
       |  , CONSTRAINT "fk_relatedFlatId"
       |      FOREIGN KEY("relatedFlatId")
       |      REFERENCES "$tableName"("_id")
       |)
       |""".stripMargin
  }
  //---------------------------------------------------------------------------
  def getImageId_AndObsTypePathSQL(tableName: String, path: String) = {
    s"""
       |SELECT "_id","observingType"
       |FROM "$tableName"
       |WHERE
       | "_path" LIKE '%$path%'
       |ORDER BY "_id" DESC LIMIT 1""".stripMargin
  }
  //---------------------------------------------------------------------------
  private def getImageId_PartialPathSQL(tableName: String, path: String) = {
    s"""
       |SELECT "_id"
       |FROM "$tableName"
       |WHERE
       | "_path" LIKE '%$path%'
       |ORDER BY "_id" DESC LIMIT 1""".stripMargin
  }
  //---------------------------------------------------------------------------
  private def getImagePathByIdSQL(tableName: String, id: String) = {
    s"""
       |SELECT "_path"
       |FROM "$tableName"
       |WHERE
       | "_id" = $id
       |ORDER BY "_path" DESC LIMIT 1""".stripMargin
  }
  //---------------------------------------------------------------------------
  def getFirstInsertedIdSQL(tableName: String) =
    s"""
       |SELECT "_id"
       |FROM "$tableName"
       |ORDER BY "_id" ASC LIMIT 1""".stripMargin
  //---------------------------------------------------------------------------
  def getLastInsertedIdSQL(tableName: String) =
    s"""
       |SELECT "_id"
       |FROM "$tableName"
       |ORDER BY "_id" DESC LIMIT 1""".stripMargin
  //---------------------------------------------------------------------------
  def getSessionNotScienceInTimeRangeSQL(
     topLeft: String
   , topRight: String) =
    s"""
       |SELECT *
       |FROM "$calibrationTableName"
       |WHERE
       | "observingType" != '$SCIENCE_NAME'
       | AND "observingTimeStamp" BETWEEN TO_DATE('$topLeft','YYYY-MM-DD') AND TO_DATE('$topRight','YYYY-MM-DD')""".stripMargin
  //---------------------------------------------------------------------------
  def getSessionInTimeRangeSQL(
      includeObservingNightDateStart: String
    , includeObservingNightDateEnd: String) =
    s"""
       |SELECT *
       |FROM "$calibrationTableName"
       |WHERE
       | "observingTimeStamp" BETWEEN TO_DATE('$includeObservingNightDateStart','YYYY-MM-DD') AND TO_DATE('$includeObservingNightDateEnd','YYYY-MM-DD')""".stripMargin
  //---------------------------------------------------------------------------
  def getSessionWithBitPixSQL(bitPix: String) =
    s"""
       |SELECT *
       |FROM "${tf.mainTableName}"
       |WHERE
       | "BITPIX" ='$bitPix'""".stripMargin

  //---------------------------------------------------------------------------
  def getUpdateFlagInCalibrationTableSQL(path: String, flags: String) =
    s"""
       |Update "$calibrationTableName"
       |SET "flags" = $flags
       |WHERE
       | "_path" = '$path'""".stripMargin
  //---------------------------------------------------------------------------
  def getCalibratedScienceImage(tableName: String, minID: String) = {
    s"""
       |SELECT "_path"
       |FROM "$tableName"
       |WHERE
       | "_id" > $minID and "observingType"= 'science'""".stripMargin
  }
  //---------------------------------------------------------------------------
  def getPath(sql: String) = {
    var rs : ResultSet = null
    val result = ArrayBuffer[String]()
    Try {
      rs = db.select(sql)
      while(rs.next) {
        result += rs.getString("_path")
      }
    }
    match {
      case Success(_) =>
        rs.close
        result
      case Failure(e) =>
        rs.close
        error(e.getMessage + s". Error processing results of sql '$sql' in PostgreDB")
        result
    }
  }
  //---------------------------------------------------------------------------
  def getImageID(path: String,tableName: String = tf.mainTableName, isPartialPath: Boolean = false) = {
    val sql = if (isPartialPath) getImageId_PartialPathSQL(tableName,path) else getImageId_ByPathSQL(tableName,path)
    ObservingManager.getImageID(sql)
  }
  //---------------------------------------------------------------------------
  def getImageIdAndObsType(path: String, tableName: String = tf.mainTableName): Option[(Int,String)] = {
    var rs : ResultSet = null
    val sql = getImageId_AndObsTypePathSQL(tableName,path).trim
    var id : Int = -1
    var obsType = ""
    Try {
      rs = db.select(sql)
      while(rs.next) {
        id = rs.getString("_id").toInt
        obsType = rs.getString("observingType")
      }
    }
    match {
      case Success(_) =>
        rs.close
        if (id == -1)  None
        else Some((id,obsType))
      case Failure(e) =>
        rs.close
        error(e.getMessage + s". Error processing results of sql '$sql' in PostgreDB")
        None
    }
  }
  //---------------------------------------------------------------------------
  def getImagePath(id: String): Option[String] = {
    var rs : ResultSet = null
    val sql = getImagePathByIdSQL(tf.mainTableName, id).trim
    var path = ""
    Try {
      rs = db.select(sql)
      while(rs.next) { path = rs.getString("_path") }
    }
    match {
      case Success(_) =>
        rs.close
        Some(path)
      case Failure(e) =>
        rs.close
        error(e.getMessage + s". Error processing results of sql '$sql' in PostgreDB")
        None
    }
  }
  //---------------------------------------------------------------------------
  def getMedianDoubleSeq(dataSeq: Array[Array[Double]], xPixSize: Int, yPixSize: Int) : Array[Double] = {
    val pixCount = xPixSize * yPixSize
    val itemCount = dataSeq.length
    (for(i <-0 until pixCount) yield {
      val sortedSeq = dataSeq.map (data=> data(i)).sorted
      val median =
        if (itemCount % 2 == 1) sortedSeq(itemCount / 2)
        else {
          val posRight = itemCount / 2
          val posLeft = posRight - 1
          (sortedSeq(posRight) + sortedSeq(posLeft)) / 2.0d
        }
      median
    }).toArray
  }

  //---------------------------------------------------------------------------
  def getMedianDoubleSeq(dataSeq: IndexedSeq[Double]): Double = {
    val itemCount = dataSeq.length
    val sortedSeq = dataSeq.sorted
    val median =
      if (itemCount % 2 == 1) sortedSeq(itemCount / 2)
      else {
        val posRight = itemCount / 2
        val posLeft = posRight - 1
        (sortedSeq(posRight) + sortedSeq(posLeft)) / 2.0d
      }
    median
  }
  //---------------------------------------------------------------------------
  def addToTableIfNotExist(pf: CalibratedFits): Unit = {
    val rowExist = db.exist(ObservingManager.getImageId_ByPathSQL(calibrationTableName,pf.masterFileName))
    if (!rowExist) addToTable(pf)
  }
  //---------------------------------------------------------------------------
  def updateFlagCalibrationTable(path: String, flags: String): Unit =
    db.update(getUpdateFlagInCalibrationTableSQL(path,flags))
  //---------------------------------------------------------------------------
  def addToTable(pf: CalibratedFits): Unit = {
    val keySeq = (calibrationTableColumnName map {"\"" + _ + "\""}).mkString("(", ", ", ")")
    val fixedValueSeq = Seq(Time.getSQL_TimeStamp, localhost, user)
    val valueSeq = (fixedValueSeq ++ pf.getValueSeq()).map (s => s"""'${s.trim}'""" )
    val valueFinalSeq = valueSeq.mkString("(", ", ", ")").replaceAll("'NULL'", "NULL")
    val sql = "INSERT INTO" + s""" "$calibrationTableName" """ + keySeq + s" VALUES $valueFinalSeq"
    if (!db.insert(sql)) {
      error(s"Columns-Value sequence:\n${(calibrationTableColumnName zip valueSeq).mkString("\n")}")
      None
    }
  }
  //---------------------------------------------------------------------------
  def getClosestMaster(sourcePf: CalibratedFits
                       , masterSeq: ConcurrentLinkedQueue[CalibratedFits]
                       , matchByFilter: Boolean = false
                       , matchByExpTimeAndTemp: Option[(Double,Double)] = None) = {
    //-------------------------------------------------------------------------
    var minDayDif = Long.MaxValue
    var minParsedFits : CalibratedFits = null
    val observingNightDate = sourcePf.observingNightDate
    //-------------------------------------------------------------------------
    def matchByDayDiff(pf: CalibratedFits) = {
      val dayDiff = Math.abs(ChronoUnit.DAYS.between(observingNightDate, pf.observingNightDate))
      if ((dayDiff >= 0) && (dayDiff <= maxDayDifferenceForMatching)) {
        if (dayDiff < minDayDif) {
          minDayDif = dayDiff
          minParsedFits = pf
        }
      }
    }
    //-------------------------------------------------------------------------
    masterSeq.forEach { masterPf =>
      if (sourcePf.canMatch(masterPf)) {
        if (matchByExpTimeAndTemp.isEmpty) {
          if (!matchByFilter || (matchByFilter && (sourcePf.filter == masterPf.filter)))
            matchByDayDiff(masterPf)
        }
        else { //match by exposure time and temperature
          if (!sourcePf.temperature.isEmpty) {
            val minExpTime = masterPf.exposureTime.toDouble - matchByExpTimeAndTemp.get._1
            val maxExpTime = masterPf.exposureTime.toDouble + matchByExpTimeAndTemp.get._1

            val minTemp = masterPf.temperature.toDouble - matchByExpTimeAndTemp.get._2
            val maxTemp = masterPf.temperature.toDouble + matchByExpTimeAndTemp.get._2

            if ((sourcePf.exposureTime.toDouble >= minExpTime) &&
                (sourcePf.exposureTime.toDouble <= maxExpTime) &&
                (sourcePf.temperature.toDouble >= minTemp) &&
                (sourcePf.temperature.toDouble <= maxTemp))

              matchByDayDiff(masterPf)
          }
        }
      }
    }
    if(minParsedFits == null) None
    else Some(minParsedFits)
  }

  //---------------------------------------------------------------------------
  def checkCalibrationTable(): Unit = {
    calibrationTableName =  tf.getQualifiedTableName(CALIBRATION_NAME)
    if (!db.existTable(tf.databaseName,calibrationTableName)) {
      info(s"Creating table : '$calibrationTableName' ")
      db.createTable(tableDefinition(calibrationTableName))
    }
    else info(s"Adding to table : '$calibrationTableName' ")
    calibrationTableColumnName = (db.getColumnNameSeq(calibrationTableName) map (_.name)).filter( s=> s!= "_id" ).toArray
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
import com.om.calibration.Calibration._
case class Calibration(om: ObservingManager) extends MyLogger {
  //---------------------------------------------------------------------------
  calibrate()
  //---------------------------------------------------------------------------
  private def checkOutputDir(obsType: OBSERVING_TYPE) = {
    obsType match {
      case BIAS    => Path.ensureDirectoryExist(outputImageDir + BIAS.toString + Path.FileSeparator)
      case DARK    => Path.ensureDirectoryExist(outputImageDir + DARK.toString + Path.FileSeparator)
      case FLAT    => Path.ensureDirectoryExist(outputImageDir + FLAT.toString + Path.FileSeparator)
      case SCIENCE => Path.ensureDirectoryExist(outputImageDir + SCIENCE.toString + Path.FileSeparator)
      case _ =>
    }
  }
  //---------------------------------------------------------------------------
  //calculate in parallel the single partitions with the same detector size
  private def calibrateParallelObservingNight(
      obsType: OBSERVING_TYPE
    , observingNightSeq: Array[ArrayBuffer[Int]]
    , coreCount: Int
    , verbose: Boolean) = {
    //-------------------------------------------------------------------------
    class MyParallelObservingNightSeq(seq: Array[Array[Int]]) extends ParallelTask[Array[Int]](
      seq
      , coreCount
      , isItemProcessingThreadSafe = true
      , verbose = verbose) {
      //-----------------------------------------------------------------------
      def userProcessSingleItem(posSeq: Array[Int]) = {
        for (i <- posSeq) {
          val idSeq = observingNightSeq(i).toArray
          Try {
            val parsedFitsSeq = idSeq.map(CalibratedFits.map(_)).sortWith( _.masterFileName < _.masterFileName)

            //avoid double calibration of bias, flat and dark
            val filteredParsedFitsSeq = parsedFitsSeq.filter(pf=> !pf.path.contains("master") )

            obsType match {
              case BIAS    => BiasCalibration.calibrate(filteredParsedFitsSeq)
              case DARK    => DarkCalibration.calibrate(filteredParsedFitsSeq)
              case FLAT    => FlatCalibration.calibrate(filteredParsedFitsSeq)
              case _ =>
            }
          }
          match {
            case Success(_) =>
            case Failure(e) =>
              error("Method: 'Calibration.userProcessSingleItem' " + e.getMessage)
              error(e.getStackTrace.mkString("\n"))
              error(s"Unexpected error processing partition: '${idSeq.mkString(",")}'. Continuing with the next partition")
          }
        }
      }
      //-----------------------------------------------------------------------
    }
    val seq = Array.tabulate(observingNightSeq.length)(n=>n)
    val partitionSeq = MyMath.getPartitionInt(0,seq.length-1,coreCount)
    Calibration.calibrateImageCount = partitionSeq.length
    new MyParallelObservingNightSeq(partitionSeq)
  }
  //---------------------------------------------------------------------------
  //calculate in parallel all science images
  private def calibrateParallelImageSeq(
    imageIdSeq: Array[Int]
    , coreCount: Int
    , verbose: Boolean) = {
    //-------------------------------------------------------------------------
    class MyParallelImageSeq(seq: Array[Int]) extends ParallelTask[Int](
      seq
      , coreCount
      , isItemProcessingThreadSafe = true
      , verbose = verbose) {
      //-----------------------------------------------------------------------
      def userProcessSingleItem(v: Int) = {
        Try {ScienceCalibration.calibrate(CalibratedFits.map(v))}
        match {
          case Success(_) =>
          case Failure(e) =>
            error("Method: 'Calibration.userProcessSingleItem' " + e.getMessage + " " + e.getStackTrace.mkString("\n"))
            error(s"Unexpected error processing partition: '$v'. Continuing with the next partition")
        }
      }
      //-----------------------------------------------------------------------
    }
    //-------------------------------------------------------------------------
    Calibration.calibrateImageCount = imageIdSeq.length
    new MyParallelImageSeq(imageIdSeq)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def loadPreviousMasterFromCalibrationTable(topLeft: LocalDate, topRight: LocalDate): Unit = {
    info(s"Loading previous masters images with observing night from '$topLeft' to: '$topRight'")
    val sql = getSessionNotScienceInTimeRangeSQL(
            topLeft.toString
          , topRight.toString)
    CalibratedFits.loadFromDatabase(sql,keepName = true).foreach(CalibratedFits.addToMasterStorage(_))
  }
  //---------------------------------------------------------------------------
  def calibrate(coreCount: Int = CPU.getCoreCount()
                , verbose: Boolean = true): Boolean = {

    checkCalibrationTable()
    info("Loading and classifying FITS headers from the database")

    //load previous session parsed images from main table
    CalibratedFits.loadSessionFromMainTable(om.sessionFirstRowID, om.sessionLastRowID) //load current sessions rows

    //load previous master images in a time range of 'maxDayDifferenceForMatching'
    if (CalibratedFits.sessionFirstObservingNightDate == null) {
      error("There is no valid night dates")
      return false
    }
    loadPreviousMasterFromCalibrationTable(CalibratedFits.sessionFirstObservingNightDate.minusDays(maxDayDifferenceForMatching)
                                          , CalibratedFits.sessionLastObservingNightDate.plusDays(maxDayDifferenceForMatching))
    //group the images by resolution (pix size)
    OBSERVING_TYPE_APPLICATION_SEQ foreach { obsType =>
      checkOutputDir(obsType)
      val partitionMap =
        obsType match {
          case BIAS    => CalibratedFits.biasImagePartitionMap
          case DARK    => CalibratedFits.darkImagePartitionMap
          case FLAT    => CalibratedFits.flatImagePartitionMap
          case SCIENCE => CalibratedFits.scienceImagePartitionMap
          case _       => null
        }
      calibrateImageTotalCount = 0
      partitionMap.keySet.foreach { dim =>
        calibrateImageCount = 0
        val observingNightMap = partitionMap(dim)
        val sampleParsedFits = CalibratedFits.map(observingNightMap.head._2.head)
        info(s"Calibrating by '$obsType' images with pix dimensions: $dim, bitPix:${sampleParsedFits.bitPix}")
        if (obsType == SCIENCE)
          calibrateParallelImageSeq(observingNightMap.values.toArray.flatten,coreCount,verbose)
        else
          calibrateParallelObservingNight(obsType,observingNightMap.values.toArray,coreCount,verbose)
        info(s"End of calibration by '$obsType' and  $dim, bitPix:${sampleParsedFits.bitPix}. Image calibration count $calibrateImageCount")
        calibrateImageTotalCount += calibrateImageCount
      }
      info(s"End of calibration by '$obsType'. Image calibration total count: $calibrateImageTotalCount")
    }

    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Calibration.scala
//=============================================================================