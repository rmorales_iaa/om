/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  24/Feb/2021
  * Time:  09h:08m
  * Description: None
  */
//=============================================================================
package com.om.calibration
//=============================================================================
import BuildInfo.BuildInfo
import com.common.fits.simpleFits.SimpleFits.{KEY_OM_RUN_DATE, KEY_OM_VER}
import com.common.configuration.MyConf
import com.common.logger.MyLogger
import com.common.util.file.MyFile
import com.om.calibration.Calibration.{CSV_FIELD_DIVIDER, getMedianDoubleSeq}
import com.common.image.myImage.MyImage
import com.common.util.time.Time
import com.om.TableDefinition.BIAS_NAME
//=============================================================================
import java.util.concurrent.ConcurrentLinkedQueue
//=============================================================================
object BiasCalibration extends MyLogger {
  //---------------------------------------------------------------------------
  val masterBiasSeq = new ConcurrentLinkedQueue[CalibratedFits]()
   //---------------------------------------------------------------------------
  private final val biasMaxValidExposureTime = MyConf.c.getFloat("Calibration.exposureTime.biasMaxValidValue")
  private final val biasMaxImagePerObservingNight = MyConf.c.getFloat("Calibration.maxImagePerObservingNight.biasMaxValue")
  //---------------------------------------------------------------------------
  private val biasPattern =
    s""".*bias.*|
       |.*zero.*|
       |.*dark.*""".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
  private def isValid(v: String) : Boolean = {
    val mv = v.toLowerCase.trim
    mv match {
      case biasPattern(_*) =>  true
      case _               =>  false
    }
  }
  //---------------------------------------------------------------------------
  def isBias(obj: String, imgType: String, obsType: String, expTime: Float, path: String) : Boolean = {
    if (expTime > biasMaxValidExposureTime) return false
    if (!isValid(path) &&
        !isValid(obj) &&
        !isValid(obsType) &&
        !isValid(imgType)) return false
    true
  }
  //---------------------------------------------------------------------------
  def getClosestMaster(pf: CalibratedFits) =
     Calibration.getClosestMaster(pf,masterBiasSeq)
  //---------------------------------------------------------------------------
  def calibrate(parsedFitsSeq: Array[CalibratedFits]): Unit = {

    if (parsedFitsSeq.length == 0) return

    val telescopeMap = CalibratedFits.groupByTelescope(parsedFitsSeq)
    for ((telescope, pfTelescopeGroupSeq) <- telescopeMap) {
      val instrumentMap = CalibratedFits.groupByInstrument(pfTelescopeGroupSeq.toArray)
      for ((instrument, pfSeq) <- instrumentMap) {
        if (pfSeq.length > biasMaxImagePerObservingNight) {
          fatal(s"Error the number of valid images per night: $biasMaxImagePerObservingNight in a '$BIAS_NAME' calibration has been exceeded with ${pfSeq.length}")
          info("Image seq starts")
          pfSeq.foreach(pf => info("\t" + pf.path + "->" + pf.observingNightDate))
          info("Image seq ends. Aborting")
          System.exit(2)
        }
        val sampleParsedFits = pfSeq.head
        info(s"Calibration by '$BIAS_NAME' the telescope: '$telescope' instrument: '$instrument' observing night: ${sampleParsedFits.observingNightDate.toString} with: ${pfSeq.length} images")

        if (MyFile.fileExist(sampleParsedFits.masterFileName)) {
          warning(s"Master BIAS image '${sampleParsedFits.masterFileName}' exists, avoiding its calculation")
          return
        }
        val imageSeq = MyImage((pfSeq map (_.path)).toArray, checkWcs = false)
        val masterBiasData = getMedianDoubleSeq(imageSeq map ( _.data), sampleParsedFits.xPixSize, sampleParsedFits.yPixSize)

        //create master bias
        new MyImage(sampleParsedFits.xPixSize
                    , sampleParsedFits.yPixSize
                    , masterBiasData
                    , userFits = Some(imageSeq.head.getSimpleFits()))
          .saveAsFits(sampleParsedFits.masterFileName
                      , sampleParsedFits.getBasicFitsHeader(BIAS_NAME).toArray
                      , _bScale = 1
                      , _bZero = 0
                      , buildInfo = Array((KEY_OM_VER, s"'${BuildInfo.version}'")
                                        , (KEY_OM_RUN_DATE, s"'${Time.getISO_DateTimeStamp}'")))

        sampleParsedFits.sourceImageIdSeq = (pfSeq map (pf=> pf.mainTableRelatedId.toString)).mkString(CSV_FIELD_DIVIDER)

        Calibration.addToTableIfNotExist(sampleParsedFits)
        masterBiasSeq.add(sampleParsedFits)
      }
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file BiasCalibration.scala
//=============================================================================
