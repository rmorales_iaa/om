/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  24/Feb/2021
  * Time:  09h:08m
  * Description: None
  */
//=============================================================================
package com.om.calibration
//=============================================================================
import BuildInfo.BuildInfo
import com.common.fits.simpleFits.SimpleFits.{KEY_OM_RUN_DATE, KEY_OM_VER}
import com.common.configuration.MyConf
import com.common.image.myImage.MyImage
import com.common.logger.MyLogger
import com.common.util.file.MyFile
import com.common.util.time.Time
import com.om.TableDefinition.DARK_NAME
import com.om.calibration.Calibration.{CSV_FIELD_DIVIDER, getMedianDoubleSeq}
//=============================================================================
import java.util.concurrent.ConcurrentLinkedQueue
import java.time.temporal.ChronoUnit
//=============================================================================
object DarkCalibration extends MyLogger {
  //---------------------------------------------------------------------------
  val masterDarkSeq = new ConcurrentLinkedQueue[CalibratedFits]()
  //---------------------------------------------------------------------------
  private final val darkMaxImagePerObservingNight   = MyConf.c.getFloat("Calibration.maxImagePerObservingNight.darkMaxValue")
  //---------------------------------------------------------------------------
  private final val exposureTimeMatchMarginSeconds  = MyConf.c.getDouble("Calibration.darkCalibration.matchMargin.exposureTimeSeconds")
  private final val temperatureMatchMarginCelsius   = MyConf.c.getDouble("Calibration.darkCalibration.matchMargin.temperatureDegreeCelsius")
  //---------------------------------------------------------------------------
  private final val  maxMatchDistance = MyConf.c.getDouble("Calibration.darkCalibration.matchMaxDistance")
  //---------------------------------------------------------------------------
  private final val  elapsedDaysWeight      = MyConf.c.getDouble("Calibration.darkCalibration.axisWeight.elapsedDays")
  private final val  exposureTimeDiffWeight = MyConf.c.getDouble("Calibration.darkCalibration.axisWeight.exposureTimeDiff")
  private final val  temperatureDiffWeight  = MyConf.c.getDouble("Calibration.darkCalibration.axisWeight.temperatureDiff")
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
  private final val itemDivider = "#"
  //---------------------------------------------------------------------------
  private val darkPattern =
    s""".*dark.*""".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
  private def isValid(s: String) : Boolean = {
    val mv = s.toLowerCase.trim
    mv match {
      case darkPattern(_*) =>  true
      case _               =>  false
    }
  }
  //---------------------------------------------------------------------------
  def isDark(obj: String
             , imgType: String
             , obsType: String
             , expTime: Float
             , path: String
             , temperature:Option[Float]) : Boolean = {

    if (expTime == 0) return false
    if (!temperature.isDefined) return false

    if (!isValid(path) &&
        !isValid(obj) &&
        !isValid(obsType) &&
        !isValid(imgType)) return false
    true
  }

  //---------------------------------------------------------------------------
  def getClosestMaster(sourcePf: CalibratedFits): Option[CalibratedFits] = {
    val r = Calibration.getClosestMaster(sourcePf
                                         , masterDarkSeq
                                         , matchByExpTimeAndTemp = Some((exposureTimeMatchMarginSeconds,temperatureMatchMarginCelsius)))

    if (r.isDefined) r
    else {
      val observingNightDate = sourcePf.observingNightDate
      val exposureTime = sourcePf.exposureTime.toDouble
      val temperature = if(sourcePf.temperature.isEmpty) Double.MaxValue else sourcePf.temperature.toDouble
      var minDistance = Double.MaxValue
      var minMasterDark: CalibratedFits = null

      //calculate the "distance" in a 3D space regarding a master dark: (elapsed days,exposure time,temperature)
      masterDarkSeq.forEach { masterDark =>

        val x = ChronoUnit.DAYS.between(observingNightDate, masterDark.observingNightDate) * elapsedDaysWeight
        val y = (exposureTime - masterDark.exposureTime.toDouble) * exposureTimeDiffWeight
        val z = (temperature - masterDark.temperature.toDouble)  * temperatureDiffWeight

        val distance = Math.sqrt( (x * x) + (y * y) + (z * z))
        if (distance < minDistance) {
          minDistance = distance
          minMasterDark = masterDark
        }
      }
      //check the min distance
      if (minDistance < maxMatchDistance) Some(minMasterDark)
      else None
    }
  }

  //---------------------------------------------------------------------------
  def calibrate(parsedFitsSeq: Array[CalibratedFits]): Unit = {

    if (parsedFitsSeq.length == 0) return

    //group by exposure time
    val exposureTimeMap = CalibratedFits.groupByExposureTime(parsedFitsSeq, exposureTimeMatchMarginSeconds)
    for ((exposureTime, pfExposureTimeGroupSeq) <- exposureTimeMap) {

      //group by instrument
      val instrumentMap = CalibratedFits.groupByInstrument(pfExposureTimeGroupSeq.toArray)
      for ((instrument, instrumentSeq) <- instrumentMap) {

        //group by temperature
        val darkTemperatureMap = CalibratedFits.groupByTemperature(instrumentSeq.toArray, temperatureMatchMarginCelsius)
        for ((_, pfSeq) <- darkTemperatureMap) {

          //fixme  group by gain
          //val gainMap = CalibratedFits.groupByTemperature(darkTemperatureMap.toArray, temperatureMatchMarginCelsius)
          //for ((_, gainSeq) <- darkTemperatureMap) {

            if (pfSeq.length > darkMaxImagePerObservingNight) {
            fatal(s"Error the number of valid images per night: $darkMaxImagePerObservingNight in a '$DARK_NAME' calibration has been exceeded with ${pfSeq.length}")
            info("Image seq starts")
            pfSeq.foreach(pf => info("\t" + pf.path + "->" + pf.observingNightDate))
            info("Image seq ends. Aborting")
            System.exit(2)
          }

          val sampleParsedFits = pfSeq.head
          info(s"Calibration by '$DARK_NAME' the telescope: '$instrument' with exposure time: $exposureTime (s) obs night: ${sampleParsedFits.observingNightDate.toString}")

          if (MyFile.fileExist(sampleParsedFits.masterFileName)) {
            warning(s"Master DARK image '${sampleParsedFits.masterFileName}' exists, avoiding its calculation")
            return
          }

          val imageSeq = MyImage((pfSeq map (_.path)).toArray, checkWcs = false)
          val masterDarkData = getMedianDoubleSeq(imageSeq map (_.data), sampleParsedFits.xPixSize, sampleParsedFits.yPixSize)

          //create master dark
          new MyImage(sampleParsedFits.xPixSize
                     , sampleParsedFits.yPixSize
                     , masterDarkData
                     , userFits = Some(imageSeq.head.getSimpleFits()) )
            .saveAsFits(sampleParsedFits.masterFileName
                        , sampleParsedFits.getBasicFitsHeader(DARK_NAME).toArray
                        , buildInfo = Array((KEY_OM_VER, s"'${BuildInfo.version}'")
                                          , (KEY_OM_RUN_DATE, s"'${Time.getISO_DateTimeStamp}'")))

          sampleParsedFits.sourceImageIdSeq = (pfSeq map (pf => pf.mainTableRelatedId.toString)).mkString(CSV_FIELD_DIVIDER)

          Calibration.addToTableIfNotExist(sampleParsedFits)
          masterDarkSeq.add(sampleParsedFits)
        }
      }
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file DarkCalibration.scala
//=============================================================================
