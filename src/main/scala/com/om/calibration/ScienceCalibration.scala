/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  24/Feb/2021
  * Time:  09h:08m
  * Description: None
  */
//=============================================================================
package com.om.calibration
//=============================================================================
import BuildInfo.BuildInfo
import com.common.fits.simpleFits.SimpleFits.{KEY_OM_RUN_DATE, KEY_OM_VER}
import com.common.util.time.Time
import com.common.fits.simpleFits.SimpleFits.KEY_OM_FLAGS
import com.common.geometry.matrix.matrix2D.Matrix2D
import com.common.image.myImage.MyImage
import com.common.util.file.MyFile
import com.om.TableDefinition.SCIENCE_NAME
import com.common.image.filter.Filter
import com.common.logger.{MyLogger, ThreadSafeLogger}
import com.common.stat.StatDescriptive
//=============================================================================
import scala.util.{Failure, Success, Try}
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object ScienceCalibration extends MyLogger {
  //---------------------------------------------------------------------------
  private final val MIN_VALID_EXPOSURE_TIME = 0.2f
  //---------------------------------------------------------------------------
  private final val INVALID_BIAS_BY_MEDIAN  = "INVALID_BIAS_BY_MEDIAN"
  //---------------------------------------------------------------------------
  final val IGNORED_FILE_LOG  = "output/om_run_ignored_file.csv"
  //---------------------------------------------------------------------------
  private val invalidSciencePattern =
    s""".*bias.*|
       |.*dark.*|
       |.*flat.*|
       |.*test.*|
       |.*zero.*""".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
  private def isValid(v: String) : Boolean = {
    val mv = v.toLowerCase.trim
    mv match {
      case invalidSciencePattern(_*) => false
      case _                         => true
    }
  }
  //---------------------------------------------------------------------------
  def isScience(obj: String, imgType: String,  expTime: Float, path: String) : Boolean = {
    if (expTime < MIN_VALID_EXPOSURE_TIME) return false
    if (!isValid(obj)) return false
    if (!isValid(imgType)) return false
    if (!isValid(path)) return false
    true
  }
  //---------------------------------------------------------------------------
  private def calibrate(img: MyImage
                        , masterBias: MyImage
                        , masterDark: MyImage
                        , masterFlat: MyImage) = {
    var xMax = img.xMax
    var yMax = img.yMax

    //priority on master dark if it exists
    val subtractMaster= {
      if (masterDark != null) {
        xMax = Math.min(xMax,masterDark.xMax) //some time size mismatch
        yMax = Math.min(yMax,masterDark.yMax)
        masterDark
      }
      else
        if (masterBias != null) {
          xMax = Math.min(xMax,masterBias.xMax) //some time size mismatch
          yMax = Math.min(yMax,masterBias.yMax)
          masterBias
        }
        else null
    }


    if (masterFlat != null) {
      xMax = Math.min(xMax, masterFlat.xMax) //some time size mismatch
      yMax = Math.min(yMax, masterFlat.yMax)
    }

    val newData = for(y<-0 until yMax; x<-0 until xMax) yield {
      val imagePix = img(x,y)
      val subtractPix = if (subtractMaster == null) 0d else subtractMaster(x, y)
      val flatPix  = if (masterFlat == null) 1d else masterFlat(x,y)
      val calPix = (imagePix - subtractPix) / flatPix
      val v =
        if (calPix >= 0) { if (calPix > img.maxValidPixelValue) img.maxValidPixelValue else calPix}  //fix maxValue allowed
        else img.minValidPixelValue
      v
    }

    MyImage(
      Matrix2D(xMax
               , yMax
               , newData.toArray)
      , img.getSimpleFits())
  }
  //---------------------------------------------------------------------------
  private def isValidBiasRegardingMedian(img: MyImage, masterBias: MyImage) = {
    if (masterBias  == null) true
    else {
      val imgMedian = img.getSimpleStats().median
      val biasMedian = StatDescriptive.getWithDouble(masterBias.data).median

      if ((imgMedian * 1.03) < biasMedian) {  //3 %
        val message = s" invalid bias. Image median: $imgMedian. Master bias median: $biasMedian. Ignoring master bias and master flat"
        warning(s"Image: '${img.name}' has a" + message)
        ThreadSafeLogger(IGNORED_FILE_LOG).add(img.name + "\t" + message)
        false
      }
      else true
    }
  }
  //---------------------------------------------------------------------------
  private def isValidBias(img: MyImage, masterBias: MyImage) : String = {
    if (!isValidBiasRegardingMedian(img, masterBias)) INVALID_BIAS_BY_MEDIAN
    else ""
  }
  //---------------------------------------------------------------------------
  private def getFlags(img: MyImage
                       , masterBias: MyImage
                       , masterDark: MyImage
                       , masterFlat: MyImage
                       , sourcePF: CalibratedFits
                       , masterDarkPF: Option[CalibratedFits]): String = {

    //get the flags
    var flags =
      (if (masterBias != null) s"Bias;" else "") +
      (if (masterDark != null) s"Dark;" else "") +
      (if (masterFlat != null) s"Flat;" else "")

    val flagInvalidBiasFlag = isValidBias(img, masterBias)
    if (!flagInvalidBiasFlag.isEmpty) flags = flagInvalidBiasFlag
    else {
        if (masterDarkPF.isDefined) {
          val expTimeDiff = sourcePF.exposureTime - masterDarkPF.get.exposureTime
          val tempDiff = sourcePF.temperature.toDouble - masterDarkPF.get.temperature.toDouble
          flags +=  s"Master dark diff (expT,T):(${f"$expTimeDiff%.1f"},${f"$tempDiff%.1f"})"
        }
    }

    if (flags.isEmpty) flags = "NOT calibrated by om"
    if (flags.endsWith(";")) flags = flags.dropRight(1)
    s"'$flags'"
  }
  //---------------------------------------------------------------------------
  def calibrate(sourcePF: CalibratedFits): Unit = {
    if (!MyFile.fileExist(sourcePF.path)) {
      val message = "path does not exist in the local storage but it exists in the database. Can not calibrate. Ignoring it"
      error(s"Image : '${sourcePF.path}' $message")
      ThreadSafeLogger(IGNORED_FILE_LOG).add(sourcePF.path + "\t" + message)
      return
    }
    if (sourcePF.filter == Filter.IGNORED_FILTER_NAME) {
      val message = s"invalid filter, assuming 'IGNORED' filter name"
      warning(s"Science calibration image '${sourcePF.masterFileName}' has $message")
      ThreadSafeLogger(IGNORED_FILE_LOG).add(sourcePF.path + "\t" + message)
    }

    if (MyFile.fileExist(sourcePF.masterFileName)) {
     warning(s"Science calibration image '${sourcePF.masterFileName}' exists, avoiding its calculation")
     return
   }

    //load the image
    val img = MyImage(sourcePF.path)

    //get and check master bias,flat and dark
    val masterBiasPF = BiasCalibration.getClosestMaster(sourcePF)
    val masterDarkPF = DarkCalibration.getClosestMaster(sourcePF)
    val masterFlatPF = FlatCalibration.getClosestMaster(sourcePF)

    var masterBias = if (masterBiasPF.isEmpty) null else MyImage(masterBiasPF.get.masterFileName)
    var masterDark = if (masterDarkPF.isEmpty) null else MyImage(masterDarkPF.get.masterFileName)
    var masterFlat = if (masterFlatPF.isEmpty) null else MyImage(masterFlatPF.get.masterFileName)

    //if master BIAS is wrong then do not calibrate
    val flagInvalidBiasFlag = isValidBias(img,masterBias)
    if (!flagInvalidBiasFlag.isEmpty) {
      masterBias = null
      masterDark = null
      masterFlat = null
    }

    //get the flags
    val flags = getFlags(img, masterBias, masterDark, masterFlat, sourcePF, masterDarkPF)

    //build the header
    val fitsHeader = ArrayBuffer(img.getSimpleFits.getKeyValueMap.toSeq:_*)
    val userFitsHeader = sourcePF.getBasicFitsHeader(SCIENCE_NAME)
    userFitsHeader += ((KEY_OM_FLAGS, s"'$flags'"))
    fitsHeader ++= userFitsHeader

    //calibrate and save
    Try {
      calibrate(img, masterBias, masterDark, masterFlat)
        .saveAsFits(sourcePF.masterFileName
          , _bScale = img.bScale
          , _bZero = img.bZero
          , userFitsRecordSeq = fitsHeader.toArray
          , userFitsRecordCommentSeq = img.getSimpleFits().getKeyValueCommentMap
          , buildInfo = Array((KEY_OM_VER, s"'${BuildInfo.version}'")
            , (KEY_OM_RUN_DATE, s"'${Time.getISO_DateTimeStamp}'")))
    }
    match {
      case Success(_) =>
      case Failure(e: Exception) =>
        ThreadSafeLogger(IGNORED_FILE_LOG).add(img.name + "\t" + "error calibrating the file")
        exception(e, s"Error calibrating the file '${img.name}'")
        return
    }

    val relatedBiasID =
      if (masterBias == null) "NULL"
      else Calibration.getImageID(masterBiasPF.get.masterFileName, Calibration.calibrationTableName).toString

    val relatedDarkID =
      if (masterDarkPF.isEmpty) "NULL"
      else Calibration.getImageID(masterDarkPF.get.masterFileName, Calibration.calibrationTableName).toString

    val relatedFlatID =
      if (masterFlat == null) "NULL"
      else
        Calibration.getImageID(masterFlatPF.get.masterFileName, Calibration.calibrationTableName).toString

    sourcePF.sourceImageIdSeq = sourcePF.mainTableRelatedId.toString
    sourcePF.relatedBiasId = relatedBiasID
    sourcePF.relatedDarkId = relatedDarkID
    sourcePF.relatedFlatId = relatedFlatID

    Calibration.addToTableIfNotExist(sourcePF)
    Calibration.updateFlagCalibrationTable(sourcePF.masterFileName, flags)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file ScienceCalibration.scala
//=============================================================================