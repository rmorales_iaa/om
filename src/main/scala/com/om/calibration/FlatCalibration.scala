/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  24/Feb/2021
  * Time:  09h:08m
  * Description: None
  */
//=============================================================================
package com.om.calibration
//=============================================================================
import BuildInfo.BuildInfo
import com.common.configuration.MyConf
import com.common.fits.simpleFits.SimpleFits.{KEY_OM_FLAGS, KEY_OM_RUN_DATE, KEY_OM_VER}
import com.common.hardware.cpu.CPU
import com.common.image.myImage.MyImage
import com.common.logger.MyLogger
import com.common.util.file.MyFile
import com.common.util.parallelTask.ParallelTask
import com.common.util.time.Time
import com.om.TableDefinition.FLAT_NAME
import com.om.calibration.Calibration.{CSV_FIELD_DIVIDER, getMedianDoubleSeq}

import scala.jdk.CollectionConverters.CollectionHasAsScala
//=============================================================================
import java.util.concurrent.ConcurrentLinkedQueue
//=============================================================================
object FlatCalibration extends MyLogger {
  //---------------------------------------------------------------------------
  val masterFlatSeq = new ConcurrentLinkedQueue[CalibratedFits]()
   //---------------------------------------------------------------------------
  private val centredImagePercentageUsable = MyConf.c.getInt("Calibration.centredImagePercentageUsable")
  //---------------------------------------------------------------------------
  private final val flatMaxValidExposureTime = MyConf.c.getFloat("Calibration.exposureTime.flatMaxValidValue")
  private final val flatMaxImagePerObservingNight = MyConf.c.getFloat("Calibration.maxImagePerObservingNight.flatMaxValue")
  //---------------------------------------------------------------------------
  private val flatPattern = ".*flat.*|.*lamp.*|.*sky.?ff.*|.*dome.*".r
  private val invalidFlatPattern =
    s""".*bias.*|
       |.*dark.*""".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
  private def isValid(v: String) : Boolean = {

    val mv = v.toLowerCase.trim
    mv match {
      case flatPattern(_*) =>         return true
      case invalidFlatPattern(_*) =>  return false
      case _ =>                       return false
    }
    false
  }
  //---------------------------------------------------------------------------
  def isFlat(obj: String, imgType: String, obsType: String, expTime: Float, path: String) : Boolean = {
    if (expTime > flatMaxValidExposureTime) return false
    if (!isValid(obj)  &&
        !isValid(imgType) &&
        !isValid(obsType) &&
        !isValid(path)
    ) false
    else true
  }
  //---------------------------------------------------------------------------
  private def getFlatNormalization(flatImageSeq: Seq[MyImage]
                                   , masterBiasPF: Option[CalibratedFits]) : Array[Double] = {
    val img = flatImageSeq.head
    val masterBias = if (masterBiasPF.isEmpty) null else MyImage(masterBiasPF.get.masterFileName)
    val pixCount = img.getPixCount
    val normalizedImageSeq = flatImageSeq map { img =>

      val correctedPix =
        for(i <-0 until pixCount) yield
          img.data(i) - (if (masterBias == null) 0d else masterBias.data(i))
      val median =
        if ((centredImagePercentageUsable < 2) || centredImagePercentageUsable > 99) getMedianDoubleSeq(correctedPix)
        else{
          val subImage = new MyImage(img.xMax
                                     , img.yMax
                                     , correctedPix.toArray
                                     , userFits = Some(flatImageSeq.head.getSimpleFits()))
            .getCentralSubImage(centredImagePercentageUsable/2)
          getMedianDoubleSeq(subImage.data)
        }
      val normalizedPix = for(i <-0 until pixCount) yield correctedPix(i) / median
      normalizedPix.toArray
    }
    getMedianDoubleSeq(normalizedImageSeq.toArray
                       , img.xMax
                       , img.yMax)
  }
  //---------------------------------------------------------------------------
  def getClosestMaster(pf: CalibratedFits) =
    Calibration.getClosestMaster(pf,masterFlatSeq,matchByFilter = true)

  //---------------------------------------------------------------------------
  private def loadImageInParallel(imageSeq: Array[String]) = {
    //-------------------------------------------------------------------------
    val loadedImageSeq = new ConcurrentLinkedQueue[MyImage]()
    //-------------------------------------------------------------------------
    class MyParallelTask() extends ParallelTask[String](
      imageSeq
      , threadCount = CPU.getCoreCount()
      , isItemProcessingThreadSafe = true
      , verbose = false
      , message = Some("loading image")) {
      //-----------------------------------------------------------------------
      def userProcessSingleItem(imagePath: String) {
         val imgSeq = MyImage(Array(imagePath), checkWcs = false)
          if (imgSeq != null && !imgSeq.isEmpty) {
            loadedImageSeq.add(imgSeq.head)
          }
      }
      //-----------------------------------------------------------------------
    }
    //-------------------------------------------------------------------------
    new MyParallelTask()
    loadedImageSeq.asScala.toArray
  }
  //---------------------------------------------------------------------------
  def calibrate(parsedFitsSeq: Array[CalibratedFits]): Unit = {

    if (parsedFitsSeq.length == 0) return

    val telescopeMap = CalibratedFits.groupByTelescope(parsedFitsSeq)
    for ((telescope, pfTelescopeGroupSeq) <- telescopeMap) {
      val instrumentMap = CalibratedFits.groupByInstrument(pfTelescopeGroupSeq.toArray)
      for ((instrument, pfInstrumentGroupSeq) <- instrumentMap) {
        val filterMap = CalibratedFits.groupByFilter(pfInstrumentGroupSeq.toArray)
        for ((filter, pfSeq) <- filterMap) {

          if (pfSeq.length > flatMaxImagePerObservingNight) {
            fatal(s"Error the number of valid images per night: $flatMaxImagePerObservingNight in a '$FLAT_NAME' calibration has been exceeded with ${pfSeq.length}")
            info("Image seq starts")
            pfSeq.foreach(pf => info("\t" + pf.path + "->" + pf.observingNightDate))
            info("Image seq ends. Aborting")
            System.exit(2)
          }

          val sampleParsedFits = pfSeq.head
          info(s"Calibration by '$FLAT_NAME' the telescope: '$telescope' instrument: '$instrument' with filter: '$filter' observing night: ${sampleParsedFits.observingNightDate.toString} with: ${pfSeq.length} images")
          if (MyFile.fileExist(sampleParsedFits.masterFileName)) {
            warning(s"Master FLAT image '${sampleParsedFits.masterFileName}' exists, avoiding its calculation")
            return
          }

          val masterBias = BiasCalibration.getClosestMaster(sampleParsedFits)
          val imageSeq = loadImageInParallel (pfSeq.toArray map (_.path))
          val masterFlatData = getFlatNormalization(imageSeq, masterBias)
          val userFitsHeader = sampleParsedFits.getBasicFitsHeader(FLAT_NAME)
          userFitsHeader += ((KEY_OM_FLAGS, if (masterBias.isDefined) s"'Bias'" else ""))

          //create master flat
          val fits = imageSeq.head.getSimpleFits()

          new MyImage(sampleParsedFits.xPixSize
                      , sampleParsedFits.yPixSize
                      , masterFlatData
                      , userFits = Some(fits))
              .saveAsFits(sampleParsedFits.masterFileName
                        , fits.getHeaderRecordSeq().toArray
                        , _bitPix = -32
                        , _bScale = 1
                        , _bZero = 0 //bit pix of master flat result image
                        , buildInfo = Array((KEY_OM_VER, s"'${BuildInfo.version}'")
                                          , (KEY_OM_RUN_DATE, s"'${Time.getISO_DateTimeStamp}'")))

          val relatedBiasID =
            if (masterBias.isEmpty) "NULL"
            else Calibration.getImageID(masterBias.get.masterFileName, Calibration.calibrationTableName).toString

          val relatedDarkID = "NULL"

          sampleParsedFits.relatedBiasId = relatedBiasID
          sampleParsedFits.relatedDarkId = relatedDarkID
          sampleParsedFits.sourceImageIdSeq = (pfSeq map (pf=> pf.mainTableRelatedId.toString)).mkString(CSV_FIELD_DIVIDER)

          Calibration.addToTableIfNotExist(sampleParsedFits)
          masterFlatSeq.add(sampleParsedFits)
        }
      }
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file FlatCalibration.scala
//=============================================================================
