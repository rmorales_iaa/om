/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  21/Feb/2021
  * Time:  11h:54m
  * Description: None
  */
//=============================================================================
package com.om
//=============================================================================
import com.common.configuration.MyConf
import com.om.ObservingType._
//=============================================================================
//=============================================================================
object TableDefinition {
  //---------------------------------------------------------------------------
  final val BIAS_NAME         = "bias"
  final val DARK_NAME         = "dark"
  final val FLAT_NAME         = "flat"
  final val SCIENCE_NAME      = "science"
  final val CALIBRATION_NAME  = "calibration"
  //---------------------------------------------------------------------------
  def getMainTableWithYear (year: String) =
    if (!year.isEmpty) year + "_" + MyConf.c.getString("Database.mainTable")
    else MyConf.c.getString("Database.mainTable")
  //---------------------------------------------------------------------------
}
//=============================================================================
import TableDefinition._
case class TableDefinition(
    databaseName: String = MyConf.c.getString("Database.database")
  , mainTableName: String) {
  //---------------------------------------------------------------------------
  def getQualifiedTableName(s: String): String = s"$mainTableName.$s"
  //---------------------------------------------------------------------------
  def toTableName(obsType: OBSERVING_TYPE) = {
    obsType match {
      case BIAS        =>  BIAS_NAME
      case DARK        =>  DARK_NAME
      case FLAT        =>  FLAT_NAME
      case SCIENCE     =>  SCIENCE_NAME
      case CALIBRATION =>  CALIBRATION_NAME
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file TableDefinition.scala
//=============================================================================
