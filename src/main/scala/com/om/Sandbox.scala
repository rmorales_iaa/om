/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  05/Mar/2021
  * Time:  08h:52m
  * Description: None
  */
//=============================================================================
package com.om
//=============================================================================
import com.common.configuration.MyConf
import com.common.image.filter.SloanG
import com.common.logger.MyLogger
import com.common.util.mail.MailAgent
import com.common.util.path.Path
import com.om.calibration.{CalibratedFits, Calibration}
import com.om.sync.RemoteDirectorySync
//=============================================================================
//=============================================================================
object Sandbox extends  MyLogger {
  //---------------------------------------------------------------------------
  def getInfo(fileName:String): Unit ={

    CalibratedFits.reportImageInfo(fileName)

    //single pattern
    val pattern = SloanG().filterPattern
   /* val subType = "n20"
    val pattern =  s"""decam_$subType.*|
                      |decam_$subType.*""".stripMargin.replaceAll("\n", "").r

    val value= "DECam_N20".toLowerCase.trim*/
   /*val value= "Sloan_g".toLowerCase

    value  match {
      case pattern(_*)  => println("TRUE")
      case _            => println("KK")
    }*/

    println(1)

    //general case
    //val r = ParsedFits.getObservingType(obj,imgType, obsType,expTime, path)

    //println(r)
  }
  //---------------------------------------------------------------------------
  def sendMail() = {
    val dirList = Seq("a","b","c")
    val filename  = "/home/rafa/proyecto/m2/build.sbt"

    val mailAgent = MailAgent()
    val mailFrom = "rmorales@iaa.es"
    val mailTo = "rmorales@iaa.es"
    val mailCC = "nicolas@iaa.es"

    val mailContent = "--- List of uncalibrated directories \n" + dirList.mkString("\n") + "\n--- List of uncalibrated directories ends"
    mailAgent.send(mailTo
      , mailFrom
      , s"[om]: Found:${dirList.size} not calibrated directories in dss16.iaa.es"
      , mailContent
      , mailCC
      , filename = Some(filename))
    true
  }
  //---------------------------------------------------------------------------
  def runCalibration() = {

    val databaseName = "om_test"
    val tableName    = "2024"
    val localDir     = "/home/rafa/Downloads/deleteme/om/caha123/20240410/"
    val resultDir    = Path.resetDirectory("/home/rafa/Downloads/deleteme/om/calibrating")

    RemoteDirectorySync(databaseName,tableName).calibrateDirAndSolveAstrometry(localDir, resultDir)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import com.om.Sandbox._
case class Sandbox() {
  //---------------------------------------------------------------------------
  MyConf.c = MyConf()

  val tf = TableDefinition(mainTableName = MyConf.c.getString("Database.mainTable") )

  val oDir = Path.ensureEndWithFileSeparator("output/calibration/image_seq")
  Path.ensureDirectoryExist(oDir)

  val om = ObservingManager(tf, oDir, enableSSH_Connection = false)
  Calibration.db = om.getDB()

  //sendMail()
 // runCalibration()
  getInfo("/mnt/uxmal_groups/image_storage/dss16_disk3/observaciones/2025/OSN15/20250110/2013LU28-0001I.fit")

  om.close()
  //---------------------------------------------------------------------------
}
//=============================================================================

//=============================================================================
//End of file Sandbox.scala
//=============================================================================
