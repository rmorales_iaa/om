/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  17/Feb/2021
  * Time:  14h:47m
  * Description: None
  */
//=============================================================================
package com.om
//=============================================================================
//=============================================================================
object ObservingType {
  //---------------------------------------------------------------------------
  final val OBSERVING_TYPE_BIAS_NAME        = "bias"
  final val OBSERVING_TYPE_DARK_NAME        = "dark"
  final val OBSERVING_TYPE_FLAT_NAME        = "flat"
  final val OBSERVING_TYPE_SCIENCE_NAME     = "science"
  final val OBSERVING_TYPE_CALIBRATION_NAME = "calibration"
  //---------------------------------------------------------------------------
  sealed trait OBSERVING_TYPE
  case object BIAS        extends OBSERVING_TYPE {override def toString = OBSERVING_TYPE_BIAS_NAME}
  case object DARK        extends OBSERVING_TYPE {override def toString = OBSERVING_TYPE_DARK_NAME}
  case object FLAT        extends OBSERVING_TYPE {override def toString = OBSERVING_TYPE_FLAT_NAME}
  case object SCIENCE     extends OBSERVING_TYPE {override def toString = OBSERVING_TYPE_SCIENCE_NAME}
  case object CALIBRATION extends OBSERVING_TYPE {override def toString = OBSERVING_TYPE_CALIBRATION_NAME}
  //----------------------------------------------------------------------------
  val OBSERVING_TYPE_APPLICATION_SEQ = Seq(
      BIAS
    , DARK
    , FLAT
    , SCIENCE)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ObservingType.scala
//=============================================================================
