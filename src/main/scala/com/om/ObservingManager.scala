/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  27/Jan/2021
  * Time:  17h:55m
  * Description: None
  */
//=============================================================================
package com.om
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.postgreDB.PostgreRawDB
import com.common.fits.simpleFits.SimpleFits
import com.common.fits.simpleFits.SimpleFits._
import com.common.image.myImage.MyImage
import com.common.logger.MyLogger
import com.common.math.MyMath
import com.common.ssh.MySSH
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
import com.common.util.time.Time
import com.common.util.time.Time.localDateTimeFormatterWithMillis
import com.om.TableDefinition.{BIAS_NAME, CALIBRATION_NAME, DARK_NAME, FLAT_NAME, SCIENCE_NAME}
import com.om.calibration.Calibration.{CSV_FIELD_DIVIDER, db, error, getImagePath}
import com.om.calibration.{CalibratedFits, Calibration}
import com.common.hardware.cpu.CPU
import com.common.util.util.Util
//=============================================================================
import java.io.File
import java.net.InetAddress
import scala.util.{Failure, Success, Try}
import java.sql.ResultSet
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME
import java.time.temporal.ChronoUnit
import java.time.format.DateTimeFormatter
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object ObservingManager {
  //---------------------------------------------------------------------------
  def PATH_FIELD_NAME = "_path"
  //---------------------------------------------------------------------------
  private val fixedKeySeq= Seq("_user", "_ip", "_timestamp",PATH_FIELD_NAME)
  //---------------------------------------------------------------------------
  //Add manually fields to be kept in the final table
  private val tableUserFixedFieldSeq =
  (CalibratedFits.PARSED_FITS_COL_SEQ flatMap (s=>  if ((s != PATH_FIELD_NAME) && (s!=  "_id")) Some(s,"VARCHAR(71)") else None)) ++
    Seq(
      ("_id" ,"SERIAL PRIMARY KEY NOT NULL")
    , ("_user" ,"VARCHAR(256) NOT NULL")
    , ("_ip" ,"VARCHAR(256) NOT NULL")
    , ("_timestamp" ,"TIMESTAMP NOT NULL")
    , (PATH_FIELD_NAME,"VARCHAR(1024) NOT NULL")
  )
  //---------------------------------------------------------------------------
  def getExistByPathSQL(tableName: String, path: String) = {
    s"""
       |SELECT "_id"
       |FROM "$tableName"
       |WHERE
       | "_path" ILIKE '%$path%'""".stripMargin
  }
  //---------------------------------------------------------------------------
  def getFirstImageIdWithPathPrefixSQL(tableName: String, prefix: String) = {
    s"""
       |SELECT "_id"
       |FROM "$tableName"
       |WHERE
       | "_path" ILIKE '$prefix%'
       |ORDER BY "_id" ASC LIMIT 1""".stripMargin
  }
  //---------------------------------------------------------------------------
  def getLastImageIdWithPathPrefixSQL(tableName: String, prefix: String) = {
    s"""
       |SELECT "_id"
       |FROM "$tableName"
       |WHERE
       | "_path" ILIKE '$prefix%'
       |ORDER BY "_id" DESC LIMIT 1""".stripMargin
  }
  //---------------------------------------------------------------------------
  def getImageId_ByPathSQL(tableName: String, path: String) = {
    s"""
       |SELECT "_id"
       |FROM "$tableName"
       |WHERE
       | "_path" = '$path'
       |ORDER BY "_id" DESC LIMIT 1""".stripMargin
  }
  //---------------------------------------------------------------------------
  def existTable(dName: String, tName: String) : Boolean = db.existTable(dName, tName)
  //---------------------------------------------------------------------------
  private def getImageInfoByIdSQL(id: String) = {
    s"""
       |SELECT "relatedBiasId" ,"relatedDarkId", "relatedFlatId", "sourceImageIdSeq", "_timestamp", "_path"
       |FROM "${Calibration.calibrationTableName}"
       |WHERE
       | "_id" = $id
       |ORDER BY "_id" DESC LIMIT 1""".stripMargin
  }
  //---------------------------------------------------------------------------
  private def getImageInfoById(id: String) = {
    var rs : ResultSet = null
    val sql = getImageInfoByIdSQL(id)
    var relatedBiasId = ""
    var relatedDarkId = ""
    var relatedFlatId = ""
    var sourceImageIdSeq : Seq[String] = null
    var timestamp: LocalDateTime = null
    var path = ""
    Try {
      rs = db.select(sql)
      while(rs.next) {
        relatedBiasId = rs.getString("relatedBiasId")
        relatedDarkId = rs.getString("relatedDarkId")
        relatedFlatId = rs.getString("relatedFlatId")
        sourceImageIdSeq = rs.getString("sourceImageIdSeq").split(CSV_FIELD_DIVIDER)
        val timeStampString = rs.getString("_timestamp")
        timestamp =
          if (timeStampString.isEmpty || timeStampString == "NULL") null
          else LocalDateTime.parse(rs.getString("_timestamp").replace(" ","T"), ISO_LOCAL_DATE_TIME)
        path = rs.getString("_path")
      }
    }
    match {
      case Success(_) =>
        rs.close()
        Some((relatedBiasId,relatedDarkId,relatedFlatId,sourceImageIdSeq,timestamp,path))
      case Failure(e) =>
        rs.close()
        error(e.getMessage + s". Error processing results of sql '$sql' in PostgreDB")
        error(e.getStackTrace.mkString("\n"))
        None
    }
  }
  //---------------------------------------------------------------------------
  def getDatabaseTypeFromRecordValue(value: String) = "VARCHAR(71)" //80-9
  //---------------------------------------------------------------------------
  def getImageID(sql: String) = {
    var rs : ResultSet = null
    var id : Int = -1
    Try {
      rs = db.select(sql)
      while(rs.next) { id = rs.getString("_id").toInt }
    }
    match {
      case Success(_) =>
        rs.close()
        id
      case Failure(e) =>
        rs.close()
        error(e.getMessage + s". Error processing results of sql '$sql' in PostgreDB")
        error(e.getStackTrace.mkString("\n"))
        -1
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import ObservingManager._
case class ObservingManager(
  tf: TableDefinition
  , outputImageDir: String
  , enableDatabase: Boolean = true
  , enableSSH_Connection: Boolean = true) extends MyLogger {
  //---------------------------------------------------------------------------
  private val db  = if (enableDatabase) connectWithDatabase() else null
  private val user = MyConf.c.getString("Database.name")
  private val localhost = InetAddress.getLocalHost
  private val ssh = if (enableSSH_Connection)
    new MySSH(MyConf.c.getString("SSH.remoteHost")
      , MyConf.c.getInt("SSH.remotePort")
      , MyConf.c.getString("SSH.user")
      , MyConf.c.getString("SSH.keyfile"))
  else null
  //---------------------------------------------------------------------------
  var sessionFirstRowID = -1
  var sessionLastRowID = -1
  //-------------------------------------------------------------------------
  //stats
  private var fileCount: Long = 0
  private var fileSizeMiBCount: Long = 0
  //---------------------------------------------------------------------------
  private val avoidRecordKeySeq    = MyConf.c.getStringSeq("FITS.avoidRecordKeySeq")
  private val fitsFileExtensionSeq = MyConf.c.getStringSeq("Util.fitsFileExtensionAllowed")
  //---------------------------------------------------------------------------
  private val recordKeyTypeMap = scala.collection.mutable.Map[String, String]()
  //---------------------------------------------------------------------------
  tableUserFixedFieldSeq foreach (p => recordKeyTypeMap(p._1) = p._2)
  //---------------------------------------------------------------------------
  def close() = {
    if (db != null) db.close()
    if (ssh != null) ssh.close()
    info("Closing 'Observing Manager'")
  }
  //---------------------------------------------------------------------------
  def getDB() = db
  //---------------------------------------------------------------------------
  def getFileCount() = fileCount
  //---------------------------------------------------------------------------
  def calculateFileCount() =
    fileCount =  db.count(s"SELECT COUNT(*) FROM \"${tf.mainTableName}\"")
  //---------------------------------------------------------------------------
  private def connectWithDatabase() : PostgreRawDB  = {
    var db = PostgreRawDB(tf.databaseName, tf.mainTableName)
    if(!db.isConnected) {
      db.close()
      warning(s"Checking the database '${tf.databaseName}'")
      db = PostgreRawDB("postgres")
      if (!db.existDatabase(tf.databaseName)) {
        warning(s"Database '${tf.databaseName}' does not exist, creating it")
        db.createDatabase(tf.databaseName)
        db.close()
        connectWithDatabase()
      }
      else {
        error(s"Database '${tf.databaseName}' exists but can not access it")
        db.close()
        db = null
      }
    }
    db
  }
  //---------------------------------------------------------------------------
  private def avoidRecord(k: String): Boolean = {
    avoidRecordKeySeq foreach (s => if (k.trim.startsWith(s)) return true)
    false
  }
  //---------------------------------------------------------------------------
  private def createTableSeq() = {
    val tName = "\"" + tf.mainTableName + "\""
    val tableHeader = Seq(s"\nCREATE TABLE $tName(\n")
    val tableTail = Seq(")\n")

    recordKeyTypeMap ++= tableUserFixedFieldSeq
    val fieldSeq = (for ((key, value) <- recordKeyTypeMap) yield {
      s"""  "$key" $value,\n"""
    }).toSeq.sorted
    val mainFieldSeq = fieldSeq.dropRight(1)
    val lastFieldSeq = Seq(fieldSeq.last.dropRight(2)) //remove last comma

    db.dropTable(tf.mainTableName)
    db.dropTable(tf.getQualifiedTableName(CALIBRATION_NAME))
    val sql = (tableHeader ++ mainFieldSeq ++ lastFieldSeq ++ tableTail).mkString
    db.createTable(sql)
  }
  //---------------------------------------------------------------------------
  private def getDatabaseValueFromRecordValue(key: String, value: String): String = {
    val simpleKey = key.replaceAll("\"", "").replaceAll("'", "").trim
    recordKeyTypeMap(simpleKey) match {
      case "varchar" =>
        "'" + value.replaceAll("\"", "").replaceAll("'", "").trim + "'"
      case _ => value
    }
  }
  //---------------------------------------------------------------------------
  def getSimpleFits(n: String, verbose: Boolean): Option[SimpleFits] = {
    val simpleN = Path.getOnlyFilename(n)
    if (verbose) info(s"Parsing FITS file: '$simpleN'")
    val simpleFits = SimpleFits(n)
    if (!simpleFits.isValid(verbose = true)) None
    else Some(simpleFits)
  }
  //---------------------------------------------------------------------------
  private def setHierarchyValues(fits: SimpleFits) =  {
    val value_seq = (HIERARCHY_FILTER_KEY_SEQ map (fits.getStringValueOrEmptyNoQuotation( _ ))).filter(s=> !s.isEmpty )
    val value = if (value_seq.isEmpty) "" else value_seq.head //take the first non empty hierarchy filter
    fits.updateRecord(KEY_FILTER_HIERARCHY, value)
  }
  //---------------------------------------------------------------------------
  private def parseFile(path: String
                        , debug: Boolean = false
                        , verbose: Boolean = true): Unit = {
    val fits = getSimpleFits(path, verbose).getOrElse(return)
    //set the hierarchy values into the fits
    setHierarchyValues(fits)

    //filter records
    val _sortedSeq = fits.getKeyValueMap.toSeq.filter { p =>
      if (avoidRecord(p._1)) false
      else recordKeyTypeMap.contains(p._1)
    }.sorted

    val sortedSeq = _sortedSeq map { case (key,value)=>
      if(Util.isDouble(value)) (key,value)
      else (key,"'" + value.replaceAll("'","")+"'")
    }

    if (!db.exist(getImageId_ByPathSQL(tf.mainTableName,path))) {
      val keySeq = fixedKeySeq ++ (sortedSeq map (k => "\"" + s"${k._1}" + "\""))
      val keyFinalSeq = keySeq.mkString("(", ", ", ")")
      val fixedFieldValueSeq = Seq(user, localhost.toString, Time.getSQL_TimeStamp, path) map (s => s"""'${s.trim}'""")
      val additionalValueSeq = sortedSeq map (t=> if (t._2.isEmpty) "''" else t._2)
      val valueSeq = (fixedFieldValueSeq ++ additionalValueSeq).zipWithIndex.map { case (v, i) => getDatabaseValueFromRecordValue(keySeq(i), v) }
      val valueFinalSeq = valueSeq.mkString("(", ", ", ")")
      val sql = "INSERT INTO" + s""" "${tf.mainTableName}" """ + s"$keyFinalSeq VALUES $valueFinalSeq"
      if (debug) println(sql)
      else {
        if (!db.insert(sql)) {
          error(s"Error inserting data from '$path'")
          error(s"Columns-Value sequence:\n${(keySeq zip valueSeq).mkString("\n")}")
        }
      }
    }
    fileCount += 1
    fileSizeMiBCount += Math.round(fits.totalByteCount.toFloat / (1024 * 1024))
  }
  //---------------------------------------------------------------------------
  private def canParseDir(dir: String
                          , excludeDirSeq: List[String]
                          , checkIgnoredPath: Boolean = true): Boolean = {
    if (excludeDirSeq.contains(dir)) {
      warning(s"Excluding directory '$dir' from processing because it is in the 'exclude-dir-seq' program argument")
      return false
    }

    if (checkIgnoredPath && CalibratedFits.isIgnoredPath(dir)) {
      warning(s"Excluding directory '$dir' from processing because it matches with the ignore path pattern")
      return false
    }
    true
  }
  //---------------------------------------------------------------------------
  private def parseFileSeqParallel(
    fileSeq: Array[File]
    , checkIgnoredPath: Boolean
    , verbose: Boolean) = {
    //-------------------------------------------------------------------------
    class MyParallelTask(seqSeq: Array[Array[Int]]) extends ParallelTask[Array[Int]](
      seqSeq
      , threadCount = CPU.getCoreCount()
      , isItemProcessingThreadSafe = true
      , verbose = false) {
      //-----------------------------------------------------------------------
      def userProcessSingleItem(fileIndexSeq: Array[Int]) =
        for (i <- fileIndexSeq) {
          val path = fileSeq(i).getAbsolutePath
          if (checkIgnoredPath && CalibratedFits.isIgnoredPath(path)) warning(s"Excluding file: '$path' from processing because it matches with the ignore path pattern")
          Try { parseFile(path, verbose = verbose) }
           match {
             case Success(_) =>
             case Failure(e) =>
               error("Method: 'parseFileSeqParallel.userProcessSingleItem' " + e.getMessage + s". Unexpected error processing file: '${fileSeq(i).getAbsolutePath}'. Continuing with the next file in partition")
               error(e.toString)
          }
        }
      //-------------------------------------------------------------------------
    }
    val fileIndexSeqSeq = MyMath.getPartitionInt(0, fileSeq.length - 1, CPU.getCoreCount())
    new MyParallelTask(fileIndexSeqSeq)
  }
  //---------------------------------------------------------------------------
  //All files of the same directory are parsed in parallel
  def exploreDir(includeDirSeq: List[String]
                 , excludeDirSeq: List[String]
                 , checkIgnoredPath: Boolean = true
                 , verbose: Boolean = false): Boolean = {
    //-------------------------------------------------------------------------
    def parseSubDir(subDir: String): Unit = {
      info(s"Parsing directory:'$subDir'")
      if (canParseDir(Path.ensureEndWithFileSeparator(subDir), excludeDirSeq, checkIgnoredPath)) {
        if (verbose) info(s"Parsing FITS in directory: '$subDir'")
        val fileSeq = Path.getSortedFileListWithExtension(subDir,fitsFileExtensionSeq)   //no recursive
        parseFileSeqParallel(fileSeq.toArray, checkIgnoredPath, verbose)
        val subDirSeq = Path.getSortedSubDirectoryList(subDir)
        subDirSeq.foreach(newSubDir=> parseSubDir( newSubDir.getAbsolutePath ))
      }
    }
    //-------------------------------------------------------------------------
    if (enableDatabase) sessionFirstRowID = getFirstOrLastRecordID()
    includeDirSeq.sorted foreach { dir =>
      if (!Path.directoryExist(dir)) error(s"Directory '$dir' does not exist. Ignoring it")
      else {
        if (CalibratedFits.isIgnoredPath(dir)) warning(s"Directory '$dir' is included in the ignore path")
        else parseSubDir(dir)
      }
    }
    exploreDirSummary()
    if (enableDatabase) sessionLastRowID = getFirstOrLastRecordID() //get last id after parsing and adding to the main table
    true
  }
  //---------------------------------------------------------------------------
  private def exploreDirSummary() = {
    info(s"FITS files processed count   : $fileCount files")
    info(s"FITS files processed size    : $fileSizeMiBCount MiB")
    true
  }
  //---------------------------------------------------------------------------
  def getFirstOrLastRecordID(tableName: String = tf.mainTableName
                              , isLast: Boolean = true): Int = {

    if (!db.existTable(db.databaseName,tableName)) return -1

    var rs : ResultSet = null
    val sql =
      if(isLast)
        Calibration.getLastInsertedIdSQL(tableName)
      else
        Calibration.getFirstInsertedIdSQL(tableName)

    var id = 1
    Try {
      rs = db.select(sql)
      while(rs.next) { id = rs.getString("_id").toInt }
    }
    match {
      case Success(_) => rs.close
      case Failure(e) => rs.close
        error(e.getMessage + s". Error processing results of sql '$sql' in PostgreDB")
        error(e.getStackTrace.mkString("\n"))
    }
    id
  }
  //---------------------------------------------------------------------------
  def getRangeID(dir: String):(Int,Int) = {
    val first = getImageID(ObservingManager.getFirstImageIdWithPathPrefixSQL(tf.mainTableName,dir))
    val last  = getImageID(ObservingManager.getLastImageIdWithPathPrefixSQL(tf.mainTableName, dir))
    (first, last)
  }
  //---------------------------------------------------------------------------
  def calibrate(includeDirSeq: List[String]): Unit = {
    info("Calibration info:")
    info(s"\t Output image dir                : '$outputImageDir'")

    if(sessionFirstRowID == sessionLastRowID) {
      warning(s"No new FITS found after parsing the directory. Loading FITS from main table at the same directory: '${includeDirSeq.mkString("{",",","}")}")
      //get the if of the first and last row in the table
      val firstSeq = ArrayBuffer[Int]()
      val lastSeq = ArrayBuffer[Int]()
      includeDirSeq.map { dir =>
        val (first,last) = getRangeID(dir)
        firstSeq += first
        lastSeq += last
      }
      sessionFirstRowID = firstSeq.head
      sessionLastRowID = lastSeq.last
    }
    else warning("Recalibrating the main table")

    Calibration.db = db
    Calibration.tf = tf
    Calibration.outputImageDir = outputImageDir
    Calibration(this)
  }
  //---------------------------------------------------------------------------
  def showFitsRecords(recordNameSeq: Seq[String], fitsFileName: String): Unit = {
   val selectCol= if ((recordNameSeq.length == 1) &&
     (recordNameSeq(0).toLowerCase == "all" || recordNameSeq(0).toLowerCase == "allnotnull") )"*"
   else (recordNameSeq map (s=> s""""$s",""")).mkString.dropRight(1)

   val allNotNullFlag = recordNameSeq(0).toLowerCase == "allnotnull"

   val sql =
     s"""SELECT $selectCol
        |FROM "${tf.mainTableName}"
        |WHERE "_path" LIKE '%$fitsFileName%'
        |""".stripMargin

    var rs : ResultSet = null
    println(s"#------- '$fitsFileName' starts -------")
    var resultCount = 0
    Try {
      rs = db.select(sql)
      val collNameSeq= db.getColumnNameSeq(rs)
      while(rs.next) {
        resultCount += 1
        collNameSeq.zipWithIndex.foreach { case (colName,i) =>
          val value = rs.getString(i+1)
          if (allNotNullFlag) {
            if (value != null) println(s"${colName.padTo(8, ' ')} = '$value'")
          }
          else println(s"${colName.padTo(8, ' ')} = '$value'")
        }
      }
    }
    match {
      case Success(_) => rs.close()
      case Failure(e) => rs.close()
        error(e.getMessage + s". Error processing results of sql '$sql' in PostgreDB")
        error(e.getStackTrace.mkString("\n"))
    }

    if (resultCount == 0)
      println(s"#Empty result!!!. Please try another FITS file or FITS record")
    println(s"#------- '$fitsFileName' ends -------")
    rs.close()
  }
  //---------------------------------------------------------------------------
  def getInfo(partialName: String, save: Boolean): String = {
    val r = Calibration.getImageIdAndObsType(partialName,Calibration.calibrationTableName)
    if (r.isEmpty) return s"'$partialName' not found"
    val (imageID, obsType) = r.get

    val rInfo = getImageInfoById(imageID.toString)
    if (rInfo.isEmpty) return s"'$partialName' not found"
    val (sci_relatedBiasId,sci_relatedDarkId,sci_relatedFlatId,sourceImageIdSeq,dateTime,sciencePath) = rInfo.get

    var rBias : Option[(String,String,String,Seq[String],LocalDateTime,String)] = None
    var rDark : Option[(String,String,String,Seq[String],LocalDateTime,String)] = None
    var rFlat : Option[(String,String,String,Seq[String],LocalDateTime,String)] = None

    obsType match {
      case SCIENCE_NAME =>
        rBias = if (sci_relatedBiasId == null || sci_relatedBiasId == "NULL") None else getImageInfoById(sci_relatedBiasId)
        rDark = if (sci_relatedDarkId == null || sci_relatedDarkId == "NULL") None else getImageInfoById(sci_relatedDarkId)
        rFlat = if (sci_relatedFlatId == null || sci_relatedFlatId == "NULL") None else getImageInfoById(sci_relatedFlatId)
      case BIAS_NAME =>
        rBias = getImageInfoById(imageID.toString)
      case DARK_NAME =>
        rBias = if (sci_relatedBiasId == null || sci_relatedBiasId == "NULL") None else getImageInfoById(sci_relatedBiasId)
        rDark = getImageInfoById(imageID.toString)
      case FLAT_NAME =>
        rBias = if (sci_relatedBiasId == null || sci_relatedBiasId == "NULL") None else getImageInfoById(sci_relatedBiasId)
        rFlat = getImageInfoById(imageID.toString)
    }

    val (_,_,_,bias_sourceImageIdSeq,bias_dateTime,biasPath) = if (rBias.isDefined) rBias.get else (null,null,null,null,null,null)
    val (_,_,_,dark_sourceImageIdSeq,dark_dateTime,darkPath) = if (rDark.isDefined) rDark.get else (null,null,null,null,null,null)
    val (_,_,_,flat_sourceImageIdSeq,flat_dateTime,flatPath) = if (rFlat.isDefined) rFlat.get else (null,null,null,null,null,null)

    val biasNameSeq : Array[String] = if (rBias.isEmpty) Array() else (bias_sourceImageIdSeq map (s=> getImagePath(s).get)).toArray
    val darkNameSeq : Array[String] = if (rDark.isEmpty) Array() else (dark_sourceImageIdSeq map (s=> getImagePath(s).get)).toArray
    val flatNameSeq : Array[String] = if (rFlat.isEmpty) Array() else (flat_sourceImageIdSeq map (s=> getImagePath(s).get)).toArray

    val sciSourcePath = getImagePath(sourceImageIdSeq.head).get

    val result = s"#------ Image: '$sciSourcePath' start ------\n" +
      "#------ SCIENCE image seq starts ------\n" +
      s"calibrated image name     : '${ if (obsType != SCIENCE_NAME) "NO SCIENCE" else sciencePath}'\n" +
      s"source image name         : '${ if (obsType != SCIENCE_NAME) "NO SCIENCE" else sciSourcePath}'\n" +
      s"master BIAS               : '${if (rBias.isEmpty) "NO BIAS" else biasPath}'\n" +
      s"master DARK               : '${if (rDark.isEmpty) "NO DARK" else darkPath}'\n" +
      s"master FLAT               : '${if (rFlat.isEmpty) "NO FLAT" else flatPath}'\n" +
      "#------ SCIENCE image seq ends ------\n" +
      "#------ BIAS image seq starts ------\n" +
      s"master BIAS elapsed day   : '${if (rBias.isEmpty) "NO BIAS" else ChronoUnit.DAYS.between(bias_dateTime, dateTime)}'\n" +
      s"bias source image sequence:\n"+
      (if (rBias.isEmpty) "\tNO BIAS\n" else biasNameSeq.map(s=> "\t" + s + "\n").mkString) +
      "#------ BIAS image seq ends ------\n" +
      "#------ DARK image seq starts ------\n" +
      s"master DARK elapsed day   : '${if (rDark.isEmpty) "NO DARK" else ChronoUnit.DAYS.between(dark_dateTime, dateTime)}'\n" +
      s"dark source image sequence:\n"+
      (if (rDark.isEmpty) "\tNO DARK\n" else darkNameSeq.map(s=> "\t" + s + "\n").mkString) +
      "#------ DARK image seq ends ------\n" +
      "#------ FLAT image seq starts ------\n" +
      s"master FLAT elapsed day   : '${if (rFlat.isEmpty) "NO FLAT" else ChronoUnit.DAYS.between(flat_dateTime, dateTime)}'\n" +
      s"flat source image sequence:\n"+
      (if (rFlat.isEmpty) "\tNO FLAT\n" else flatNameSeq.map(s=> "\t" + s + "\n").mkString) +
      "#------ FLAT image seq ends -----\n" +
      s"#------ Image: '$sciSourcePath' ends ------\n"

    if (save) {
      val d = Path.resetDirectory("output/info/" + Path.getOnlyFilenameNoExtension(sciencePath))
      info(s"Saving related images in: '$d'")

      val scienceDir =  d+"science/"
      Path.ensureDirectoryExist(scienceDir)
      ssh.copyFromRemote(scienceDir, Array(sciSourcePath, sciencePath), keepRemotePath = false)

      if (biasNameSeq.length > 0) {
        val biasDir =  d+"bias/"
        Path.ensureDirectoryExist(biasDir)
        ssh.copyFromRemote(biasDir, biasNameSeq :+ biasPath, keepRemotePath = false)
      }

      if (darkNameSeq.length > 0) {
        val darkDir =  d+"dark/"
        Path.ensureDirectoryExist(darkDir)
        ssh.copyFromRemote(darkDir, darkNameSeq :+ darkPath, keepRemotePath = false)
      }

      if (flatNameSeq.length > 0) {
        val flatDir =  d+"flat/"
        Path.ensureDirectoryExist(flatDir)
        ssh.copyFromRemote(flatDir, flatNameSeq :+ flatPath, keepRemotePath = false)
      }
    }
    result
  }
  //---------------------------------------------------------------------------
  def addCalibrationLabelToImage(o: String, path:String) = {
    val r = CalibratedFits.getObservingInfo(path)
    val imageTpe  = r._1.getOrElse("None")
    val telescope  = r._2
    val filter     = r._3
    val exposureTime= r._4.getOrElse(1f)
    val observingTimeStamp  = r._5.get
    val instrument = r._7.replaceAll("#","_")
    val temperature = r._8
    val objectName = r._9
    val binning    = r._10
    val pixScale   = r._11
    val fovX       = r._12
    val fovY       = r._13
    val itemDivider = "#"

    val labelSeq = Array(
      (KEY_OM_MASTER, imageTpe)
      , (KEY_OM_DATEOBS, s"'${observingTimeStamp.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)}'")
      , (KEY_OM_EXPTIME, s"'${r._4.get}'")
      , (KEY_OM_FILTER, s"'$filter'")
      , (KEY_OM_TELESCOPE, s"'$telescope'")
      , (KEY_OM_INSTRUMENT, s"'$instrument'")
      , (KEY_OM_TEMPERATURE, if(temperature.isEmpty) "" else temperature.get.toString)
      , (KEY_OM_OBJECT, s"'$objectName'")
      , (KEY_OM_BINNING, s"$binning")
      , (KEY_OM_PIX_SCALE_X, s"${if (pixScale != -1) pixScale else "1"}")
      , (KEY_OM_PIX_SCALE_Y, s"${if (pixScale != -1) pixScale else "1"}")
      , (KEY_OM_FOV_X, s"${if (fovX != -1) f"$fovX%.3f" else "1"}")
      , (KEY_OM_FOV_Y, s"${if (fovY != -1) f"$fovY%.3f" else "1"}")
      , (KEY_OM_FLAGS, s"'Manual label'")
    )
    val observingType = "science"
    val img = MyImage(path)
    val fits = img.getSimpleFits()
    val xPixSize = fits.getKeyIntValue(KEY_NAXIS_1)
    val yPixSize = fits.getKeyIntValue(KEY_NAXIS_2)

    val masterFileName =
      ( observingType + itemDivider +
        s"${Time.getTimeStampWindowsCompatible(observingTimeStamp.format(localDateTimeFormatterWithMillis))}" + itemDivider +
        s"${xPixSize}x$yPixSize" + itemDivider +
        s"$filter" + itemDivider +
        s"$telescope" + itemDivider +
        s"${Math.round(exposureTime)}s"+ itemDivider +
        s"$instrument" + itemDivider +
        s"$objectName")
        .replaceAll(" ","_")
        .replaceAll("__","_")
        .replaceAll("%","_")
        .replaceAll("\\(","_")
        .replaceAll("\\)","_")
        .replaceAll("\\.","_") +
        ".fits"

    img.saveAsFitsRaw(o + masterFileName, labelSeq)
  }
  //---------------------------------------------------------------------------
  def addCalibrationLabelToDir(inDir: String, outDir:String) = {
    //-------------------------------------------------------------------------
    val o = Path.resetDirectory(outDir)
    //-------------------------------------------------------------------------
    class MyParallelImageSeq(seq: Array[String]) extends ParallelTask[String](
      seq
      , threadCount = CPU.getCoreCount()
      , isItemProcessingThreadSafe = true
      , message = Some("adding calibration labels")) {
      //-----------------------------------------------------------------------
      def userProcessSingleItem(imageName: String) =
        Try {
          addCalibrationLabelToImage(o,imageName)
        }
        match {
          case Success(_) =>
          case Failure(e) =>
            error(e.getMessage + s" Error adding label to image '$imageName' continuing with the next image")
            error(e.getStackTrace.mkString("\n"))
        }
      //-----------------------------------------------------------------------
    }
    //-------------------------------------------------------------------------
    val seq = Path.getSortedFileListRecursive(inDir,MyConf.c.getStringSeq("Util.fitsFileExtensionAllowed")).map (_.getAbsolutePath)
    new MyParallelImageSeq(seq.toArray)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ObservingManager.scala
//=============================================================================
