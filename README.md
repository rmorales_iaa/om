'om' (Observing Manager) is a program for managing and indexing for FITS image headers. It can process a directory (recursively) and parse all FITS headers (https://fits.gsfc.nasa.gov/fits_standard.html), indexing all FITS records found in the header into a relational database. 'om' has been implemented using Scala prograaming language, but a a muliplatform '.jar' file is provided.
Requirements:
a) A PostgreSQL (https://www.postgresql.org/) instance.
b) A database created in the PostgreSQL instance
Capabilities:
a) create database
Syntax: [create-table table include-remoteDir-seq exclude-remoteDir-seq]
om generates the sql sentence for creating a table  into de database. Each column is the key of any record present into FITS file
header (the value of the record will be inserted into de database using b) capability).
The FITS files are found recursively starting from 'include-remoteDir-seq' directory sequence. It is possible to avoid certain directories
setting 'exclude-remoteDir-seq'. The allowed FITS extensons can be configured in 'input/configuration/config.conf'.
All files of a subdirectory are processed in parallel. The parallelization grade (core count) can be set in 'input/configuration/config.conf'.
The symbolic links are followed if they exists. In case of recursive symbolic links, please add to 'exclude-remoteDir-seq'.
PostgreSQL has a limit of 1600 columns. When the resulting SQL sentence has more columns, please just remove some of them, The FITS file will be indexed in any case but no on the removed columns(key records),
Due the expected variabilty of exceptions in FITS format, om uses a custom permissive FITS parser. All values of the records are considered as strings in the table.
The table includes the following additional columns:
	"_filename" varchar(64) NOT NULL   #name of the FITS file
   "_id" serial NOT NULL               #sequential number
   "_ip" varchar(20) NOT NULL          #ip of the user that inserted the value   
   "_path" varchar(512) NOT NULL       #path of the file 
   "_timestamp" timestamp NOT NULL     #insertion timestamp
   "_user" varchar(64) NOT NULL        #user that inserted the row

The primary key of the table is : "_id"
b) fill database
Syntax: [fill-table table include-remoteDir-seq exclude-remoteDir-seq]
om parses recursively a sequence of directories finding FITS files. A FITS record is composed by a pair key-value. The key will be used as column in the table 'table' and the record value will be the value of the columnm
c) script
Syntax: [script scriptFile]
Execute a SQL sentence (strored in a 'scriptFile'), with the possibily of saving the images returned. The results os the SQL query is saved as a CSV file using the parameter of the script file:
OM_CSV_FILENAME = "CSV_FILENAME"
In order to save the images, the column '_path' must be in the result of the sentence and the script must include the followin options:
- OM_SAVE_FITS = 1
- OM_IMAGE_DIR = "OUTPUT_DIR"
